package com.example.springbootdemo.other;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;


@Component
/*@PropertySource(value = {"classpath:application-dev.properties"})*/
/*@ConfigurationProperties(prefix = "url")*/
@Validated
public class TestConfigurationProperties {


    private String name;
/*
    @NotBlank
    private String isNull;

    public String getIsNull() {
        return isNull;
    }

    public void setIsNull(String isNull) {
        this.isNull = isNull;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
