package com.example.springbootdemo.other;

import org.springframework.beans.factory.annotation.Value;

//@Component实体类可加可不加，不加的话必须用@Bean
public class TestSprintEl {

    @Value("${url.name}")
    private String name;




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
