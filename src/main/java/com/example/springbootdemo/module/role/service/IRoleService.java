package com.example.springbootdemo.module.role.service;

import com.example.springbootdemo.module.role.domain.Role;

import java.util.List;
import java.util.Set;

/**
 * 角色 服务层
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
public interface IRoleService 
{
	/**
     * 查询角色信息
     * 
     * @param roleId 角色ID
     * @return 角色信息
     */
	public Role selectRoleById(Integer roleId);

	public Set<String> selectRoleKeys(Integer userId);
	/**
     * 查询角色列表
     * 
     * @param role 角色信息
     * @return 角色集合
     */
	public List<Role> selectRoleList(Role role);
	
	/**
     * 新增角色
     * 
     * @param role 角色信息
     * @return 结果
     */
	public int insertRole(Role role);
	
	/**
     * 修改角色
     * 
     * @param role 角色信息
     * @return 结果
     */
	public int updateRole(Role role);

	int updateRule(Role role);

	/**
     * 删除角色信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteRoleByIds(String ids);
	
}
