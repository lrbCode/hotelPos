package com.example.springbootdemo.module.role.service;

import com.example.springbootdemo.framework.shrio.ShiroUtils;
import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.framework.utils.StringUtils;
import com.example.springbootdemo.module.role.domain.Role;
import com.example.springbootdemo.module.role.mapper.RoleMapper;
import com.example.springbootdemo.module.roleMenu.domain.RoleMenu;
import com.example.springbootdemo.module.roleMenu.mapper.RoleMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 角色 服务层实现
 *
 * @author scau_imis43
 * @date 2019-04-08
 */
@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    /**
     * 查询角色信息
     *
     * @param roleId 角色ID
     * @return 角色信息
     */
    @Override
    public Role selectRoleById(Integer roleId) {
        return roleMapper.selectRoleById(roleId);
    }

    @Override
    public Set<String> selectRoleKeys(Integer userId) {

        List<Role> perms = roleMapper.selectRolesByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (Role perm : perms) {

            if (StringUtils.isNotNull(perm)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询角色列表
     *
     * @param role 角色信息
     * @return 角色集合
     */
    @Override
    public List<Role> selectRoleList(Role role) {
        return roleMapper.selectRoleList(role);
    }

    /**
     * 新增角色
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public int insertRole(Role role) {
        return roleMapper.insertRole(role);
    }

    /**
     * 修改角色
     *
     * @param
     * @return 结果
     */
    @Override
    public int updateRole(Role role) {
        return roleMapper.updateRole(role);
    }

    /**
     * 修改角色
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public int updateRule(Role role) {

        // 修改角色信息
        ShiroUtils.clearCachedAuthorizationInfo();
        // 删除角色与菜单关联
        roleMenuMapper.deleteRoleMenuByRoleId(role.getRoleId());
        return insertRoleMenu(role);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(Role role) {
        int rows = 1;
        // 新增用户与角色管理
        List<RoleMenu> list = new ArrayList<RoleMenu>();
        for (Integer menuId : role.getMenuIds()) {
            RoleMenu rm = new RoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0) {
            rows = roleMenuMapper.batchRoleMenu(list);
        }
        return rows;
    }

    /**
     * 删除角色对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteRoleByIds(String ids) {
        return roleMapper.deleteRoleByIds(Convert.toStrArray(ids));
    }

}
