package com.example.springbootdemo.module.role.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.role.domain.Role;
import com.example.springbootdemo.module.role.service.IRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 角色 信息操作处理
 *
 * @author scau_imis43
 * @date 2019-04-08
 */
@Controller
@RequestMapping("/role")
public class RoleController extends BaseController {
    @Autowired
    private IRoleService roleService;
    private String prefix = "role/";




    @RequiresPermissions("system:role:edit")
    @PostMapping("/editRule")
    @ResponseBody
    public ServerResponse editRule(Role role) {
        int res=roleService.updateRule(role);
        return  res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }




    @GetMapping("/roleRule")
    public String roleRule() {
        return prefix + "editRule";
    }


    @RequiresPermissions("system:role:view")
    @GetMapping()
    public String role() {
        return prefix + "roleManage";
    }



    @RequestMapping("/roleEdit")
    public String openEdit(){
        return prefix + "roleEdit";
    }

    @RequiresPermissions("system:role:list")
    @GetMapping("/list")
    @ResponseBody
    public ServerResponse list(Role role) {
        List<Role> list = roleService.selectRoleList(role);
        return ServerResponse.createBySuccess(list,list.size());
    }

    @RequiresPermissions("system:role:edit")
    @PostMapping("/edit")
    @ResponseBody
    public ServerResponse editSave(Role role) {
        int res=roleService.updateRole(role);
        return  res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

    @RequiresPermissions("system:role:add")
    @PostMapping("/add")
    @ResponseBody
    public ServerResponse addSave(Role role) {

        int res = roleService.insertRole(role);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

    @RequiresPermissions("system:role:remove")
    @PostMapping("/remove")
    @ResponseBody
    public ServerResponse remove(String ids) {

        int res = roleService.deleteRoleByIds(ids);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

   /* private String prefix = "module/role";
	

	
	@RequiresPermissions("module:role:view")
	@GetMapping()
	public String role()
	{
	    return prefix + "/role";
	}
	
	*//**
     * 查询角色列表
     *//*
	@RequiresPermissions("module:role:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Role role)
	{
		startPage();
        List<Role> list = roleService.selectRoleList(role);
		return getDataTable(list);
	}
	
	
	*//**
     * 导出角色列表
     *//*
	@RequiresPermissions("module:role:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Role role)
    {
    	List<Role> list = roleService.selectRoleList(role);
        ExcelUtil<Role> util = new ExcelUtil<Role>(Role.class);
        return util.exportExcel(list, "role");
    }
	
	*//**
     * 新增角色
     *//*
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	*//**
     * 新增保存角色
     *//*
	@RequiresPermissions("module:role:add")
	@Log(title = "角色", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Role role)
	{		
		return toAjax(roleService.insertRole(role));
	}

	*//**
     * 修改角色
     *//*
	@GetMapping("/edit/{roleId}")
	public String edit(@PathVariable("roleId") Integer roleId, ModelMap mmap)
	{
		Role role = roleService.selectRoleById(roleId);
		mmap.put("role", role);
	    return prefix + "/edit";
	}
	
	*//**
     * 修改保存角色
     *//*
	@RequiresPermissions("module:role:edit")
	@Log(title = "角色", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Role role)
	{		
		return toAjax(roleService.updateRole(role));
	}
	
	*//**
     * 删除角色
     *//*
	@RequiresPermissions("module:role:remove")
	@Log(title = "角色", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(roleService.deleteRoleByIds(ids));
	}
	*/
}
