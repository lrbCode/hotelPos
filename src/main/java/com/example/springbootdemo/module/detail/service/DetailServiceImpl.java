package com.example.springbootdemo.module.detail.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.detail.domain.Detail;
import com.example.springbootdemo.module.detail.domain.DetailVo;
import com.example.springbootdemo.module.detail.mapper.DetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 客房当前详情,具体到房间号，一间房可以住多个客户 服务层实现
 * 
 * @author scau_imis43
 * @date 2019-04-06
 */
@Service
public class DetailServiceImpl implements IDetailService 
{
	@Autowired
	private DetailMapper detailMapper;

	/**
     * 查询客房当前详情,具体到房间号，一间房可以住多个客户信息
     * 
     * @param roomNumber 客房当前详情,具体到房间号，一间房可以住多个客户ID
     * @return 客房当前详情,具体到房间号，一间房可以住多个客户信息
     */
    @Override
	public Detail selectDetailById(Integer roomNumber)
	{
	    return detailMapper.selectDetailById(roomNumber);
	}
	
	/**
     * 查询客房当前详情,具体到房间号，一间房可以住多个客户列表
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 客房当前详情,具体到房间号，一间房可以住多个客户集合
     */
	@Override
	public List<DetailVo> selectDetailList(DetailVo detail)
	{
	    return detailMapper.selectDetailList(detail);
	}
	
    /**
     * 新增客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 结果
     */
	@Override
	public int insertDetail(DetailVo detail)
	{
	    return detailMapper.insertDetail(detail);
	}
	
	/**
     * 修改客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 结果
     */
	@Override
	public int updateDetail(DetailVo detail)
	{
	    return detailMapper.updateDetail(detail);
	}

	/**
     * 删除客房当前详情,具体到房间号，一间房可以住多个客户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteDetailByIds(String ids)
	{
		return detailMapper.deleteDetailByIds(Convert.toStrArray(ids));
	}
	
}
