package com.example.springbootdemo.module.detail.service;

import com.example.springbootdemo.module.detail.domain.Detail;
import com.example.springbootdemo.module.detail.domain.DetailVo;

import java.util.List;

/**
 * 客房当前详情,具体到房间号，一间房可以住多个客户 服务层
 * 
 * @author scau_imis43
 * @date 2019-04-06
 */
public interface IDetailService 
{
	/**
     * 查询客房当前详情,具体到房间号，一间房可以住多个客户信息
     * 
     * @param roomNumber 客房当前详情,具体到房间号，一间房可以住多个客户ID
     * @return 客房当前详情,具体到房间号，一间房可以住多个客户信息
     */
	public Detail selectDetailById(Integer roomNumber);
	
	/**
     * 查询客房当前详情,具体到房间号，一间房可以住多个客户列表
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 客房当前详情,具体到房间号，一间房可以住多个客户集合
     */
	public List<DetailVo> selectDetailList(DetailVo detail);
	
	/**
     * 新增客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 结果
     */
	public int insertDetail(DetailVo detail);
	
	/**
     * 修改客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 结果
     */
	public int updateDetail(DetailVo detail);
		
	/**
     * 删除客房当前详情,具体到房间号，一间房可以住多个客户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteDetailByIds(String ids);
	
}
