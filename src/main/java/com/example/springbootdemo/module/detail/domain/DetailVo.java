package com.example.springbootdemo.module.detail.domain;

public class DetailVo extends Detail {
    private String roomType;


    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }
}
