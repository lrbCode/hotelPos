package com.example.springbootdemo.module.detail.mapper;

import com.example.springbootdemo.module.detail.domain.Detail;
import com.example.springbootdemo.module.detail.domain.DetailVo;

import java.util.List;

/**
 * 客房当前详情,具体到房间号，一间房可以住多个客户 数据层
 * 
 * @author scau_imis43
 * @date 2019-04-06
 */
public interface DetailMapper 
{
	/**
     * 查询客房当前详情,具体到房间号，一间房可以住多个客户信息
     * 
     * @param roomNumber 客房当前详情,具体到房间号，一间房可以住多个客户ID
     * @return 客房当前详情,具体到房间号，一间房可以住多个客户信息
     */
	public Detail selectDetailById(Integer roomNumber);
	
	/**
     * 查询客房当前详情,具体到房间号，一间房可以住多个客户列表
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 客房当前详情,具体到房间号，一间房可以住多个客户集合
     */
	public List<DetailVo> selectDetailList(Detail detail);
	
	/**
     * 新增客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 结果
     */
	public int insertDetail(Detail detail);
	
	/**
     * 修改客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param detail 客房当前详情,具体到房间号，一间房可以住多个客户信息
     * @return 结果
     */
	public int updateDetail(Detail detail);
	
	/**
     * 删除客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param roomNumber 客房当前详情,具体到房间号，一间房可以住多个客户ID
     * @return 结果
     */
	public int deleteDetailById(Integer roomNumber);
	
	/**
     * 批量删除客房当前详情,具体到房间号，一间房可以住多个客户
     * 
     * @param roomNumbers 需要删除的数据ID
     * @return 结果
     */
	public int deleteDetailByIds(String[] roomNumbers);

    int selectEmptyById(Integer rtid);

	List<Integer> selectDetailNum(Integer rtid);
}