package com.example.springbootdemo.module.detail.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 客房当前详情,具体到房间号，一间房可以住多个客户表 room_detail
 * 
 * @author scau_imis43
 * @date 2019-04-06
 */
public class Detail
{
	private static final long serialVersionUID = 1L;
	
	/** 房间号主键 */
	private Integer roomNumber;
	/** 房间类型ID外键 */
	private Integer rtid;
	/** 是否为钟点房状态 默认0否 1是 */
	private Integer clockRoom;
	/** 钟点房，价格\小时 */
	private Float clockPrice;
	/** 客房联系电话 */
	private String roomTel;
	/** 备注 */
	private String remark;
	/** 当前房间状态 默认0(空房) 1入住  */
	private Integer curStatus;

	public void setRoomNumber(Integer roomNumber) 
	{
		this.roomNumber = roomNumber;
	}

	public Integer getRoomNumber() 
	{
		return roomNumber;
	}
	public void setRtid(Integer rtid) 
	{
		this.rtid = rtid;
	}

	public Integer getRtid() 
	{
		return rtid;
	}
	public void setClockRoom(Integer clockRoom) 
	{
		this.clockRoom = clockRoom;
	}

	public Integer getClockRoom() 
	{
		return clockRoom;
	}
	public void setClockPrice(Float clockPrice) 
	{
		this.clockPrice = clockPrice;
	}

	public Float getClockPrice() 
	{
		return clockPrice;
	}
	public void setRoomTel(String roomTel) 
	{
		this.roomTel = roomTel;
	}

	public String getRoomTel() 
	{
		return roomTel;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}
	public void setCurStatus(Integer curStatus) 
	{
		this.curStatus = curStatus;
	}

	public Integer getCurStatus() 
	{
		return curStatus;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("roomNumber", getRoomNumber())
            .append("rtid", getRtid())
            .append("clockRoom", getClockRoom())
            .append("clockPrice", getClockPrice())
            .append("roomTel", getRoomTel())
            .append("remark", getRemark())
            .append("curStatus", getCurStatus())
            .toString();
    }
}
