package com.example.springbootdemo.module.detail.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.detail.domain.DetailVo;
import com.example.springbootdemo.module.detail.service.IDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 客房当前详情,具体到房间号，一间房可以住多个客户 信息操作处理
 *
 * @author scau_imis43
 * @date 2019-04-06
 */
@Controller
@RequestMapping("/roomDetail")
public class DetailController extends BaseController {
    private String prefix = "roomDetail/";

    @Autowired
    private IDetailService detailService;

    @GetMapping()
    public String detail() {
        return prefix + "roomCheck";
    }
    @RequestMapping("/roomEdit")
    public String openEdit() {
        return prefix + "roomEdit";
    }
    @RequestMapping("/roomAdd")
    public String openAdd() {
        return prefix + "roomAdd";
    }


    /**
     * 查询客房当前详情,具体到房间号，一间房可以住多个客户列表
     */

    @GetMapping("/list")
    @ResponseBody
    public ServerResponse list(DetailVo detail) {
        List<DetailVo> list = detailService.selectDetailList(detail);
        return ServerResponse.createBySuccess(list,list.size());
    }

    /**
     * 新增客房当前详情,具体到房间号，一间房可以住多个客户
     *//*
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }
*/
    /**
     * 新增保存客房当前详情,具体到房间号，一间房可以住多个客户
     */
    @PostMapping("/addSave")
    @ResponseBody
    public ServerResponse addSave(DetailVo detail) {
        int res = detailService.insertDetail(detail);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }


    /**
     * 修改保存客房当前详情,具体到房间号，一间房可以住多个客户
     */

    @PostMapping("/edit")
    @ResponseBody
    public ServerResponse editSave(DetailVo detail) {
        int res=detailService.updateDetail(detail);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

    /**
     * 删除客房当前详情,具体到房间号，一间房可以住多个客户
     */

    @PostMapping("/remove")
    @ResponseBody
    public ServerResponse remove(String ids) {
        int res=detailService.deleteDetailByIds(ids);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

}
