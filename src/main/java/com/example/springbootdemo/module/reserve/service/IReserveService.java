package com.example.springbootdemo.module.reserve.service;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.reserve.domain.Reserve;
import com.example.springbootdemo.module.reserve.domain.ReserveVo;

import java.util.List;

/**
 * 预订记录 服务层
 * 
 * @author scau_imis43
 * @date 2019-04-23
 */
public interface IReserveService 
{

	/**
     * 查询预订记录列表
     * 
     * @param reserve 预订记录信息
     * @return 预订记录集合
     */
	public List<ReserveVo> selectReserveList(ReserveVo reserve);
	
	/**
     * 新增预订记录
     * 
     * @param reserve 预订记录信息
     * @return 结果
     */
	public ServerResponse insertReserve(ReserveVo reserve);
	
	/**
     * 修改预订记录
     * 
     * @param reserve 预订记录信息
     * @return 结果
     */
	public int updateReserve(ReserveVo reserve);
		
	/**
     * 删除预订记录信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteReserveByIds(String ids);

    int updateLive(String phone);

	int updateReserve2(Reserve reserve);
}
