package com.example.springbootdemo.module.reserve.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.reserve.domain.Reserve;
import com.example.springbootdemo.module.reserve.domain.ReserveVo;
import com.example.springbootdemo.module.reserve.service.IReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 预订记录 信息操作处理
 *
 * @author scau_imis43
 * @date 2019-04-22
 */
@Controller
@RequestMapping("/reserve")
public class ReserveController extends BaseController {
    private String prefix = "reserve/";

    @Autowired
    private IReserveService reserveService;

    @RequestMapping("/reserveManage")
    public String getIndex6() {
        return "reserve/reserveManage";
    }

    @RequestMapping("/reserveAdd")
    public String getIndex7() {
        return "reserve/reserveAdd";
    }

    @RequestMapping("/reserveEdit")
    public String getIndex8() {
        return "reserve/reserveEdit";
    }

    @RequestMapping("/reserveAdd2")
    public String getIndex9() {
        return "reserve/reserveAdd2";
    }
    @RequestMapping("/reserveCheckIn")
    public String checkIn() {
        return "reserve/reserveCheckIn";
    }

    /**
     * 查询预订记录列表
     */
//	@RequiresPermissions("reserve:list")
    @GetMapping("/list")
    @ResponseBody
    public ServerResponse list(ReserveVo reserve) {
        System.out.println(reserve.toString());
        List<ReserveVo> list = reserveService.selectReserveList(reserve);
        return ServerResponse.createBySuccess(list,list.size());

    }



    /**
     * 新增保存预订记录
     */
    //@RequiresPermissions("reserve:add")
    @PostMapping("/add")
    @ResponseBody
    public ServerResponse addSave(@RequestBody ReserveVo reserve) {


        reserve.setId((int) System.currentTimeMillis());
        return reserveService.insertReserve(reserve);
    }
    @RequestMapping("/editStatus")
    @ResponseBody
    public ServerResponse editStatus(Reserve reserve) {
        int res = reserveService.updateReserve2(reserve);

        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();

    }
    /**
     * 修改保存预订记录
     */
    //@RequiresPermissions("reserve:edit")
    @PostMapping("/edit")
    @ResponseBody
    public ServerResponse editSave(@RequestBody ReserveVo reserve) {
        int res = reserveService.updateReserve(reserve);

        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();

    }

    /**
     * 删除预订记录
     */
    //@RequiresPermissions("reserve:remove")
    @PostMapping("/remove")
    @ResponseBody
    public ServerResponse remove(String phoneIds) {
        System.out.println(phoneIds);
        int res = reserveService.deleteReserveByIds(phoneIds);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

}
