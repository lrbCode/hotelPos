package com.example.springbootdemo.module.reserve.domain;

import com.example.springbootdemo.module.reserveType.domain.ReserveType;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

public class ReserveVo extends Reserve {

    //中间表，购物车
    private List<ReserveType> reserveTypes;






    public List<ReserveType> getReserveTypes() {
        return reserveTypes;
    }

    public void setReserveTypes(List<ReserveType> reserveTypes) {
        this.reserveTypes = reserveTypes;
    }
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("phone", getPhone())
                .append("id", getId())
                .append("reservationTime", getReservationTime())
                .append("goTime", getGoTime())

                .append("delStatus", getDelStatus())
                .append("remark", getRemark())
                .append("reserveTypes",getReserveTypes())
                .toString();
    }

}
