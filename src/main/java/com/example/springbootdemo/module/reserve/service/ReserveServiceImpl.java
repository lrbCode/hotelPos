package com.example.springbootdemo.module.reserve.service;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.framework.utils.StringUtils;
import com.example.springbootdemo.module.detail.mapper.DetailMapper;
import com.example.springbootdemo.module.reserve.domain.Reserve;
import com.example.springbootdemo.module.reserve.domain.ReserveVo;
import com.example.springbootdemo.module.reserve.mapper.ReserveMapper;
import com.example.springbootdemo.module.reserveType.domain.ReserveType;
import com.example.springbootdemo.module.reserveType.mapper.ReserveTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;

/**
 * 预订记录 服务层实现
 *
 * @author scau_imis43
 * @date 2019-04-23
 */
@Service
public class ReserveServiceImpl implements IReserveService {
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private ReserveMapper reserveMapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private ReserveTypeMapper reserveTypeMapper;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private DetailMapper DetailMapper;


    /**
     * 查询预订记录列表
     *
     * @param reserve 预订记录信息
     * @return 预订记录集合
     */
    @Override
    public List<ReserveVo> selectReserveList(ReserveVo reserve) {
        return reserveMapper.selectReserveList(reserve);
    }

    /**
     * 新增预订记录
     *
     * @param reserve 预订记录信息
     * @return 结果
     */
    @Transactional
    @Override
    public ServerResponse insertReserve(ReserveVo reserve) {
        String phone = reserve.getPhone();
        List<String> phoneRepeat = reserveMapper.selectPhoneByTime(reserve);//重叠时间的预约
        List<ReserveType> temp = reserve.getReserveTypes();
        System.out.println("以下为重复的时间电话-------"+StringUtils.isEmpty(phoneRepeat));



        //若手机号从来没预约过
        if (reserveMapper.selectReserveById(phone)<=0) {

            reserveMapper.insertReserve(reserve);

        }

        if (StringUtils.isEmpty(phoneRepeat)) {
            for (ReserveType t : temp) {
                if (reserveTypeMapper.selectByRtid(t) > 0) {
                    //判断是否重复预约过相同类型的房间，若相同直接更改数量
                    return ServerResponse.createByErrorMessage("当前手机号存在已预定过的类型，请直接更改预订订单即可！类型ID："+t.getRtid());
                } else {
                    reserveTypeMapper.insertReserveType(t);
                }
            }
        }else {
            for (ReserveType t : temp) {//遍历购物车中的类型
                int orderCur = t.getOrderNum();//当前类型要预定的数量
                int orderTol = DetailMapper.selectEmptyById(t.getRtid());//当前类型的总数量
                int orderHaved = 0;//已预定的数量
                for (String phoneTemp : phoneRepeat) {
                    orderHaved += reserveTypeMapper.selectOrderNum(phoneTemp, t.getRtid().toString());
                }

                if ((orderCur + orderHaved) >= orderTol)//判断购物车中的房间类型数量是否已满
                    return ServerResponse.createByErrorMessage("购物车中存在房间类型数量余量不足！类型ID：" + t.getRtid());
                else if (reserveTypeMapper.selectByRtid(t) > 0) {
                    //判断是否重复预约过相同类型的房间，若相同直接更改数量
                    return ServerResponse.createByErrorMessage("当前手机号存在已预定过的类型，请直接更改预订订单即可！类型ID：" + t.getRtid());
                } else {
                    reserveTypeMapper.insertReserveType(t);
                }

            }
        }



        return ServerResponse.createBySuccess();
    }

    /**
     * 修改预订记录
     *
     * @param reserve 预订记录信息
     * @return 结果
     */
    @Override
    public int updateReserve(ReserveVo reserve) {
        List<ReserveType> temp = reserve.getReserveTypes();
        Iterator iterator=temp.iterator();
        while (iterator.hasNext()) {
            reserveTypeMapper.updateReserveType((ReserveType) iterator.next());
        }

        return reserveMapper.updateReserve(reserve);
    }
    public int updateReserve2(Reserve reserve) {

        return reserveMapper.updateReserveStatus3(reserve);
    }
    /**
     * 删除预订记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteReserveByIds(String ids) {
        return reserveMapper.deleteReserveByIds(Convert.toStrArray(ids));
   /*     String[] phones=Convert.toStrArray(ids);
        for (int i = 0; i < phones.length; i++) {
            reserveMapper.deleteReserveById(phones[i]);
        }
        return 1;*/
    }

    @Override
    public int updateLive(String phone) {
        int i= reserveMapper.deleteReserveById(phone);
        int j=reserveTypeMapper.deleteReserveTypeById(phone);
        return j>0&&i>0?1:0;
    }

}
