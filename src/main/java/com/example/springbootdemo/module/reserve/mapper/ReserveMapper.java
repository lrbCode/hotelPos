package com.example.springbootdemo.module.reserve.mapper;

import com.example.springbootdemo.module.reserve.domain.Reserve;
import com.example.springbootdemo.module.reserve.domain.ReserveVo;

import java.util.List;

/**
 * 预订记录 数据层
 * 
 * @author scau_imis43
 * @date 2019-04-23
 */
public interface ReserveMapper 
{
	/**
     * 查询预订记录信息
     * 
     * @param phone 预订记录ID
     * @return 预订记录信息
     */
	public int selectReserveById(String phone);
	
	/**
     * 查询预订记录列表
     * 
     * @param reserve 预订记录信息
     * @return 预订记录集合
     */
	public List<ReserveVo> selectReserveList(ReserveVo reserve);
	
	/**
     * 新增预订记录
     * 
     * @param reserve 预订记录信息
     * @return 结果
     */
	public int insertReserve(ReserveVo reserve);
	
	/**
     * 修改预订记录
     * 
     * @param reserve 预订记录信息
     * @return 结果
     */
	public int updateReserve(Reserve reserve);
	
	/**
     * 删除预订记录
     * 
     * @param phone 预订记录ID
     * @return 结果
     */
	public int deleteReserveById(String phone);
	
	/**
     * 批量删除预订记录
     * 
     * @param phones 需要删除的数据ID
     * @return 结果
     */
	public int deleteReserveByIds(String[] phones);

	int insertReserve2Type(ReserveVo reserve);

    List<String> selectPhoneByTime(ReserveVo reserve);

	int updateReserveStatus(String phone);

    int updateReserveStatus2(String phone);

	int updateReserveStatus3(Reserve reserve);
}