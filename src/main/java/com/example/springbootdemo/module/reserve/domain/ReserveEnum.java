package com.example.springbootdemo.module.reserve.domain;

public enum ReserveEnum {

    FULL(0),
    REMAIN(1);

    private final int status;

    ReserveEnum(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
