package com.example.springbootdemo.module.reserve.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 预订记录表 reserve
 * 
 * @author scau_imis43
 * @date 2019-04-23
 */
public class Reserve
{
	private static final long serialVersionUID = 1L;
	
	/** 手机号 */
	private String phone;
	/** 预定单号 随机生成 */
	private Integer id;
	/** 预订日期(天) */

	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date reservationTime;
	/** 预定退房日期(天) */
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date goTime;

	/** 状态(0已预订 1预定完成 2退订失效)，新增预订not between */
	private Integer delStatus;
	/** 备注 */
	private String remark;

	public void setPhone(String phone) 
	{
		this.phone = phone;
	}

	public String getPhone() 
	{
		return phone;
	}
	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getId() 
	{
		return id;
	}
	public void setReservationTime(Date reservationTime) 
	{
		this.reservationTime = reservationTime;
	}

	public Date getReservationTime() 
	{
		return reservationTime;
	}
	public void setGoTime(Date goTime) 
	{
		this.goTime = goTime;
	}

	public Date getGoTime() 
	{
		return goTime;
	}

	public void setDelStatus(Integer delStatus) 
	{
		this.delStatus = delStatus;
	}

	public Integer getDelStatus() 
	{
		return delStatus;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}


}
