package com.example.springbootdemo.module.roleMenu.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.roleMenu.domain.RoleMenu;
import com.example.springbootdemo.module.roleMenu.mapper.RoleMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 角色和菜单关联 服务层实现
 * 
 * @author scau_imis43
 * @date 2019-04-15
 */
@Service
public class RoleMenuServiceImpl implements IRoleMenuService 
{
	@Autowired
	private RoleMenuMapper roleMenuMapper;

	/**
     * 查询角色和菜单关联信息
     * 
     * @param roleId 角色和菜单关联ID
     * @return 角色和菜单关联信息
     */
    @Override
	public RoleMenu selectRoleMenuById(Integer roleId)
	{
	    return roleMenuMapper.selectRoleMenuById(roleId);
	}
	
	/**
     * 查询角色和菜单关联列表
     * 
     * @param roleMenu 角色和菜单关联信息
     * @return 角色和菜单关联集合
     */
	@Override
	public List<RoleMenu> selectRoleMenuList(RoleMenu roleMenu)
	{
	    return roleMenuMapper.selectRoleMenuList(roleMenu);
	}
	
    /**
     * 新增角色和菜单关联
     * 
     * @param roleMenu 角色和菜单关联信息
     * @return 结果
     */
	@Override
	public int insertRoleMenu(RoleMenu roleMenu)
	{
	    return roleMenuMapper.insertRoleMenu(roleMenu);
	}
	
	/**
     * 修改角色和菜单关联
     * 
     * @param roleMenu 角色和菜单关联信息
     * @return 结果
     */
	@Override
	public int updateRoleMenu(RoleMenu roleMenu)
	{
	    return roleMenuMapper.updateRoleMenu(roleMenu);
	}

	/**
     * 删除角色和菜单关联对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteRoleMenuByIds(String ids)
	{
		return roleMenuMapper.deleteRoleMenuByIds(Convert.toStrArray(ids));
	}
	
}
