package com.example.springbootdemo.module.roleMenu.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 角色和菜单关联表 pos_role_menu
 * 
 * @author scau_imis43
 * @date 2019-04-15
 */
public class RoleMenu
{
	private static final long serialVersionUID = 1L;
	
	/** 角色ID */
	private Integer roleId;
	/** 菜单ID */
	private Integer menuId;

	public void setRoleId(Integer roleId) 
	{
		this.roleId = roleId;
	}

	public Integer getRoleId() 
	{
		return roleId;
	}
	public void setMenuId(Integer menuId) 
	{
		this.menuId = menuId;
	}

	public Integer getMenuId() 
	{
		return menuId;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("roleId", getRoleId())
            .append("menuId", getMenuId())
            .toString();
    }
}
