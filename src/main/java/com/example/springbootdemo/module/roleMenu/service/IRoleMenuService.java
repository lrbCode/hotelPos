package com.example.springbootdemo.module.roleMenu.service;

import com.example.springbootdemo.module.roleMenu.domain.RoleMenu;
import java.util.List;

/**
 * 角色和菜单关联 服务层
 * 
 * @author scau_imis43
 * @date 2019-04-15
 */
public interface IRoleMenuService 
{
	/**
     * 查询角色和菜单关联信息
     * 
     * @param roleId 角色和菜单关联ID
     * @return 角色和菜单关联信息
     */
	public RoleMenu selectRoleMenuById(Integer roleId);
	
	/**
     * 查询角色和菜单关联列表
     * 
     * @param roleMenu 角色和菜单关联信息
     * @return 角色和菜单关联集合
     */
	public List<RoleMenu> selectRoleMenuList(RoleMenu roleMenu);
	
	/**
     * 新增角色和菜单关联
     * 
     * @param roleMenu 角色和菜单关联信息
     * @return 结果
     */
	public int insertRoleMenu(RoleMenu roleMenu);
	
	/**
     * 修改角色和菜单关联
     * 
     * @param roleMenu 角色和菜单关联信息
     * @return 结果
     */
	public int updateRoleMenu(RoleMenu roleMenu);
		
	/**
     * 删除角色和菜单关联信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteRoleMenuByIds(String ids);
	
}
