package com.example.springbootdemo.module.customer.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.customer.domain.Customer;
import com.example.springbootdemo.module.customer.service.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 入住客户登记\客户注册记录 信息操作处理
 * 
 * @author scau_imis43
 * @date 2019-05-07
 */
@Controller
@RequestMapping("/customer")
public class CustomerController extends BaseController
{
    private String prefix = "customer/";
	
	@Autowired
	private ICustomerService customerService;

	@GetMapping()
	public String customer() {
		return prefix + "customerManager";
	}
	@RequestMapping("/customerAdd")
	public String customerAdd() {
		return prefix + "customerAdd";
	}
	@RequestMapping("/customerEdit")
	public String customerEdit() {
		return prefix + "customerEdit";
	}

	@GetMapping("/list")
	@ResponseBody
	public ServerResponse list(Customer customer) {
		List<Customer> list= customerService.selectCustomerList(customer);
		return ServerResponse.createBySuccess(list,list.size());
	}

	/**
	 * 新增保存客房当前详情,具体到房间号，一间房可以住多个客户
	 */
	@PostMapping("/addSave")
	@ResponseBody
	public ServerResponse addSave(Customer customer) {
		int res = customerService.insertCustomer(customer);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}


	/**
	 * 修改保存客房当前详情,具体到房间号，一间房可以住多个客户
	 */

	@PostMapping("/edit")
	@ResponseBody
	public ServerResponse editSave(Customer customer) {
		int res=customerService.updateCustomer(customer);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}

	/**
	 * 删除客房当前详情,具体到房间号，一间房可以住多个客户
	 */

	@PostMapping("/remove")
	@ResponseBody
	public ServerResponse remove(String ids) {
		int res=customerService.deleteCustomerByIds(ids);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}

	

	

	

	
}
