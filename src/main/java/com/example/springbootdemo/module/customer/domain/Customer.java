package com.example.springbootdemo.module.customer.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 入住客户登记\客户注册记录表 customer
 * 
 * @author scau_imis43
 * @date 2019-05-07
 */
public class Customer
{
	private static final long serialVersionUID = 1L;
	
	/** 客户ID(自增) */
	private Integer id;
	/** 电话 */
	private String phone;
	/** 客户姓名 */
	private String name;
	/** 1男 0女 */
	private Integer sex;
	/** 客户身份证 */
	private String customerCard;
	/** 客户年龄 */
	private Integer age;
	/** 备注 */
	private String remark;
	/** 入住订单外键 */
	private Integer checkId;

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getId() 
	{
		return id;
	}
	public void setPhone(String phone) 
	{
		this.phone = phone;
	}

	public String getPhone() 
	{
		return phone;
	}
	public void setName(String name) 
	{
		this.name = name;
	}

	public String getName() 
	{
		return name;
	}
	public void setSex(Integer sex) 
	{
		this.sex = sex;
	}

	public Integer getSex() 
	{
		return sex;
	}
	public void setCustomerCard(String customerCard) 
	{
		this.customerCard = customerCard;
	}

	public String getCustomerCard() 
	{
		return customerCard;
	}
	public void setAge(Integer age) 
	{
		this.age = age;
	}

	public Integer getAge() 
	{
		return age;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}
	public void setCheckId(Integer checkId) 
	{
		this.checkId = checkId;
	}

	public Integer getCheckId() 
	{
		return checkId;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("phone", getPhone())
            .append("name", getName())
            .append("sex", getSex())
            .append("customerCard", getCustomerCard())
            .append("age", getAge())
            .append("remark", getRemark())
            .append("checkId", getCheckId())
            .toString();
    }
}
