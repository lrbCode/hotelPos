package com.example.springbootdemo.module.customer.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.customer.domain.Customer;
import com.example.springbootdemo.module.customer.mapper.CustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 入住客户登记\客户注册记录 服务层实现
 * 
 * @author scau_imis43
 * @date 2019-05-07
 */
@Service
public class CustomerServiceImpl implements ICustomerService 
{
	@Autowired
	private CustomerMapper customerMapper;

	/**
     * 查询入住客户登记\客户注册记录信息
     * 
     * @param id 入住客户登记\客户注册记录ID
     * @return 入住客户登记\客户注册记录信息
     */
    @Override
	public Customer selectCustomerById(Integer id)
	{
	    return customerMapper.selectCustomerById(id);
	}
	
	/**
     * 查询入住客户登记\客户注册记录列表
     * 
     * @param customer 入住客户登记\客户注册记录信息
     * @return 入住客户登记\客户注册记录集合
     */
	@Override
	public List<Customer> selectCustomerList(Customer customer)
	{
	    return customerMapper.selectCustomerList(customer);
	}
	
    /**
     * 新增入住客户登记\客户注册记录
     * 
     * @param customer 入住客户登记\客户注册记录信息
     * @return 结果
     */
	@Override
	public int insertCustomer(Customer customer)
	{
	    return customerMapper.insertCustomer(customer);
	}
	
	/**
     * 修改入住客户登记\客户注册记录
     * 
     * @param customer 入住客户登记\客户注册记录信息
     * @return 结果
     */
	@Override
	public int updateCustomer(Customer customer)
	{
	    return customerMapper.updateCustomer(customer);
	}

	/**
     * 删除入住客户登记\客户注册记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteCustomerByIds(String ids)
	{
		return customerMapper.deleteCustomerByIds(Convert.toStrArray(ids));
	}
	
}
