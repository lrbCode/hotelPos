package com.example.springbootdemo.module.customer.service;

import com.example.springbootdemo.module.customer.domain.Customer;
import java.util.List;

/**
 * 入住客户登记\客户注册记录 服务层
 * 
 * @author scau_imis43
 * @date 2019-05-07
 */
public interface ICustomerService 
{
	/**
     * 查询入住客户登记\客户注册记录信息
     * 
     * @param id 入住客户登记\客户注册记录ID
     * @return 入住客户登记\客户注册记录信息
     */
	public Customer selectCustomerById(Integer id);
	
	/**
     * 查询入住客户登记\客户注册记录列表
     * 
     * @param customer 入住客户登记\客户注册记录信息
     * @return 入住客户登记\客户注册记录集合
     */
	public List<Customer> selectCustomerList(Customer customer);
	
	/**
     * 新增入住客户登记\客户注册记录
     * 
     * @param customer 入住客户登记\客户注册记录信息
     * @return 结果
     */
	public int insertCustomer(Customer customer);
	
	/**
     * 修改入住客户登记\客户注册记录
     * 
     * @param customer 入住客户登记\客户注册记录信息
     * @return 结果
     */
	public int updateCustomer(Customer customer);
		
	/**
     * 删除入住客户登记\客户注册记录信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteCustomerByIds(String ids);
	
}
