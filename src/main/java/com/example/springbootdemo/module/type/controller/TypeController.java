package com.example.springbootdemo.module.type.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.type.domain.Type;
import com.example.springbootdemo.module.type.service.ITypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 客房类型 信息操作处理
 *
 * @date 2019-03-26
 */
@Controller
@RequestMapping("/roomType")
public class TypeController extends BaseController {
    private String prefix = "roomType/";

    @Autowired
    @Qualifier("TypeServiceImpl")
    private ITypeService typeService;

    @GetMapping()
    public String getRoomType() {
        return prefix + "roomTypeManage";
    }

    //@Log(title = "客房类型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public ServerResponse addSave(Type type) {

        int res = typeService.insertType(type);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

    //@Log(title = "客房类型", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public ServerResponse remove(String ids) {
        System.out.println(ids);
        int res = typeService.deleteTypeByIds(ids);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

    @GetMapping("/list")
    @ResponseBody
    public ServerResponse list(Type type) {
        //  System.out.println(limit+page);
        List<Type> list = typeService.selectTypeList(type);
        return ServerResponse.createBySuccess(list,list.size());
    }


    @PostMapping("/edit")
    @ResponseBody
    public ServerResponse editSave(Type type) {
        int res=typeService.updateType(type);
        return  res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }


}
