package com.example.springbootdemo.module.type.service;

import com.example.springbootdemo.module.type.domain.Type;

import java.util.List;

/**
 * 客房类型 服务层
 * 
 * @author ruoyi
 * @date 2019-03-26
 */

public interface ITypeService 
{
	/**
     * 查询客房类型信息
     * 
     * @param id 客房类型ID
     * @return 客房类型信息
     */
	public Type selectTypeById(Integer id);
	
	/**
     * 查询客房类型列表
     * 
     * @param type 客房类型信息
     * @return 客房类型集合
     */
	public List<Type> selectTypeList(Type type);
	
	/**
     * 新增客房类型
     * 
     * @param type 客房类型信息
     * @return 结果
     */
	public int insertType(Type type);
	
	/**
     * 修改客房类型
     * 
     * @param type 客房类型信息
     * @return 结果
     */
	public int updateType(Type type);
		
	/**
     * 删除客房类型信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteTypeByIds(String ids);
	
}
