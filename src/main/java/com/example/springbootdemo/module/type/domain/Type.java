package com.example.springbootdemo.module.type.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 客房类型表 room_type
 * 
 * @author
 * @date 2019-03-26
 */
public class Type
{
	private static final long serialVersionUID = 1L;
	
	/** 主键 */
	private Integer id;
	/** 客房类型 */
	private String roomType;
	/** 空房数量 */
	private Integer emptyNum;
	/** 客房类型介绍 */
	private String remark;

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getId() 
	{
		return id;
	}
	public void setRoomType(String roomType) 
	{
		this.roomType = roomType;
	}

	public String getRoomType() 
	{
		return roomType;
	}
	public void setEmptyNum(Integer emptyNum) 
	{
		this.emptyNum = emptyNum;
	}

	public Integer getEmptyNum() 
	{
		return emptyNum;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("roomType", getRoomType())
            .append("emptyNum", getEmptyNum())
            .append("remark", getRemark())
            .toString();
    }
}
