package com.example.springbootdemo.module.type.mapper;

import com.example.springbootdemo.module.type.domain.Type;
import org.springframework.stereotype.Repository;

import java.util.List;	

/**
 * 客房类型 数据层
 * 
 * @author ruoyi
 * @date 2019-03-26
 */
@Repository
public interface TypeMapper 
{
	/**
     * 查询客房类型信息
     * 
     * @param id 客房类型ID
     * @return 客房类型信息
     */
	public Type selectTypeById(Integer id);
	
	/**
     * 查询客房类型列表
     * 
     * @param type 客房类型信息
     * @return 客房类型集合
     */
	public List<Type> selectTypeList(Type type);
	
	/**
     * 新增客房类型
     * 
     * @param type 客房类型信息
     * @return 结果
     */
	public int insertType(Type type);
	
	/**
     * 修改客房类型
     * 
     * @param type 客房类型信息
     * @return 结果
     */
	public int updateType(Type type);
	
	/**
     * 删除客房类型
     * 
     * @param id 客房类型ID
     * @return 结果
     */
	public int deleteTypeById(Integer id);
	
	/**
     * 批量删除客房类型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteTypeByIds(String[] ids);

    List<Integer> selectTypeId();

    int selectTypeNum();
}