package com.example.springbootdemo.module.type.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.type.domain.Type;
import com.example.springbootdemo.module.type.mapper.TypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 客房类型 服务层实现
 * 
 * @author ruoyi
 * @date 2019-03-26
 */
@Service("TypeServiceImpl")
public class TypeServiceImpl implements ITypeService 
{

	@Autowired
	private TypeMapper typeMapper;

	/**
     * 查询客房类型信息
     * 
     * @param id 客房类型ID
     * @return 客房类型信息
     */
    @Override
	public Type selectTypeById(Integer id)
	{
	    return typeMapper.selectTypeById(id);
	}
	
	/**
     * 查询客房类型列表
     * 
     * @param type 客房类型信息
     * @return 客房类型集合
     */
	@Override
	public List<Type> selectTypeList(Type type)
	{
	    return typeMapper.selectTypeList(type);
	}
	
    /**
     * 新增客房类型
     * 
     * @param type 客房类型信息
     * @return 结果
     */
	@Override
	public int insertType(Type type)
	{
	    return typeMapper.insertType(type);
	}
	
	/**
     * 修改客房类型
     * 
     * @param type 客房类型信息
     * @return 结果
     */
	@Override
	public int updateType(Type type)
	{
	    return typeMapper.updateType(type);
	}

	/**
     * 删除客房类型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteTypeByIds(String ids)
	{
		return typeMapper.deleteTypeByIds(Convert.toStrArray(ids));
	}
	
}
