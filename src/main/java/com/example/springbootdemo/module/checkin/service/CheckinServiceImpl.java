package com.example.springbootdemo.module.checkin.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.checkin.domain.Chart1;
import com.example.springbootdemo.module.checkin.domain.ChartDomain;
import com.example.springbootdemo.module.checkin.domain.Checkin;
import com.example.springbootdemo.module.checkin.mapper.CheckinMapper;
import com.example.springbootdemo.module.detail.mapper.DetailMapper;
import com.example.springbootdemo.module.type.mapper.TypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单\入住登记 服务层实现
 *
 * @author scau_imis43
 * @date 2019-05-07
 */
@Service
public class CheckinServiceImpl implements ICheckinService {
    @Autowired
    private CheckinMapper checkinMapper;
    @Autowired
    private TypeMapper TypeMapper;
    @Autowired
    private DetailMapper detailMapper;

    /**
     * 查询订单\入住登记信息
     *
     * @param liveId 订单\入住登记ID
     * @return 订单\入住登记信息
     */
    @Override
    public Checkin selectCheckinById(Integer liveId) {
        return checkinMapper.selectCheckinById(liveId);
    }

    /**
     * 查询订单\入住登记列表
     *
     * @param checkin 订单\入住登记信息
     * @return 订单\入住登记集合
     */
    @Override
    public List<Checkin> selectCheckinList(Checkin checkin) {
        return checkinMapper.selectCheckinList(checkin);
    }

    /**
     * 新增订单\入住登记
     *
     * @param checkin 订单\入住登记信息
     * @return 结果
     */
    @Override
    public int insertCheckin(Checkin checkin) {
        return checkinMapper.insertCheckin(checkin);
    }

    /**
     * 修改订单\入住登记
     *
     * @param checkin 订单\入住登记信息
     * @return 结果
     */
    @Override
    public int updateCheckin(Checkin checkin) {
        return checkinMapper.updateCheckin(checkin);
    }

    /**
     * 删除订单\入住登记对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCheckinByIds(String ids) {
        return checkinMapper.deleteCheckinByIds(Convert.toStrArray(ids));
    }

    @Override
    public Map<String, List<Integer>> getRoomDetail() {
        List<Integer> rtids = TypeMapper.selectTypeId();
        Map<String, List<Integer>> res = new HashMap<>();
        for (Integer i : rtids) {
            List<Integer> roomNum = detailMapper.selectDetailNum(i);
            res.put(TypeMapper.selectTypeById(i).getRoomType(), roomNum);
        }

        return res;
    }

    @Override
    public List<Chart1> getChart(ChartDomain chartDomain) {

        List<Chart1> chart1 = new ArrayList<>();
        boolean isAll = chartDomain.getRtid() == -1 ? true : false;
        //需要返回自定义x坐标?前端处理
   /*     if (chartDomain.getStart() != null && chartDomain.getEnd() != null) {

        }*/

        if (isAll) {

            //查全部房间类型rtid
            List<Integer> num=checkinMapper.selectCheckRoomType();
            for (int i = 0; i < num.size(); i++) {
                Chart1 temp=new Chart1();
                temp.setName(TypeMapper.selectTypeById(num.get(i)).getRoomType());
                chartDomain.setRtid(num.get(i));
                float res=checkinMapper.selectChartZx(chartDomain);
                temp.setData(res);
                chart1.add(temp);
            }
            //需要注入多个chart1
        } else {
            float res=checkinMapper.selectChartZx(chartDomain);//查某种类型
            //只需要注入1个chart1
            Chart1 temp=new Chart1();
            temp.setName(TypeMapper.selectTypeById(chartDomain.getRtid()).getRoomType());
            temp.setData(res);
            chart1.add(temp);
        }

        return chart1;

    }

}
