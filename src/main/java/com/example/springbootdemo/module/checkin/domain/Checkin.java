package com.example.springbootdemo.module.checkin.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 订单\入住登记表 checkin
 *
 * @author scau_imis43
 * @date 2019-05-07
 */
public class Checkin {
    private static final long serialVersionUID = 1L;

    /**
     * 入住单号，随机生成
     */
    private Integer liveId;
    /**
     * 房间具体信息外键，查金额和房间号  一对一
     */
    private Integer rdid;
    /**
     * 押金
     */
    private Float cashPledge;
    /**
     * 入住人数
     */
    private Integer liveNums;
    /**
     * 当前入住日期(天),钟点房精确到小时
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date orderDate;
    /**
     * 当前退房日期(天),钟点房精确到小时
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date leaveDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标记 默认0正在入住 1已退房
     */
    private Integer delFlag;
    /**
     * 支付金额(方便计算钟点房)
     */
    private Float payment;
    /**
     * 支付状态 0未支付 1已支付
     */
    private Integer payStatus;

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLiveId() {
        return liveId;
    }

    public void setLiveId(Integer liveId) {
        this.liveId = liveId;
    }

    public Integer getRdid() {
        return rdid;
    }

    public void setRdid(Integer rdid) {
        this.rdid = rdid;
    }

    public Float getCashPledge() {
        return cashPledge;
    }

    public void setCashPledge(Float cashPledge) {
        this.cashPledge = cashPledge;
    }

    public Integer getLiveNums() {
        return liveNums;
    }

    public void setLiveNums(Integer liveNums) {
        this.liveNums = liveNums;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Float getPayment() {
        return payment;
    }

    public void setPayment(Float payment) {
        this.payment = payment;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("liveId", getLiveId())
                .append("rdid", getRdid())
                .append("cashPledge", getCashPledge())
                .append("liveNums", getLiveNums())
                .append("orderDate", getOrderDate())
                .append("leaveDate", getLeaveDate())
                .append("remark", getRemark())
                .append("delFlag", getDelFlag())
                .append("payment", getPayment())
                .append("payStatus", getPayStatus())
                .toString();
    }
}
