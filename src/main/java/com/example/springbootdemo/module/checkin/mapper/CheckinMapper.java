package com.example.springbootdemo.module.checkin.mapper;

import com.example.springbootdemo.module.checkin.domain.ChartDomain;
import com.example.springbootdemo.module.checkin.domain.Checkin;
import java.util.List;	

/**
 * 订单\入住登记 数据层
 * 
 * @author scau_imis43
 * @date 2019-05-07
 */
public interface CheckinMapper 
{
	/**
     * 查询订单\入住登记信息
     * 
     * @param liveId 订单\入住登记ID
     * @return 订单\入住登记信息
     */
	public Checkin selectCheckinById(Integer liveId);
	
	/**
     * 查询订单\入住登记列表
     * 
     * @param checkin 订单\入住登记信息
     * @return 订单\入住登记集合
     */
	public List<Checkin> selectCheckinList(Checkin checkin);
	
	/**
     * 新增订单\入住登记
     * 
     * @param checkin 订单\入住登记信息
     * @return 结果
     */
	public int insertCheckin(Checkin checkin);
	
	/**
     * 修改订单\入住登记
     * 
     * @param checkin 订单\入住登记信息
     * @return 结果
     */
	public int updateCheckin(Checkin checkin);
	
	/**
     * 删除订单\入住登记
     * 
     * @param liveId 订单\入住登记ID
     * @return 结果
     */
	public int deleteCheckinById(Integer liveId);
	
	/**
     * 批量删除订单\入住登记
     * 
     * @param liveIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteCheckinByIds(String[] liveIds);

    float selectChartZx(ChartDomain chartDomain);

	void selectChartAllZx(ChartDomain chartDomain);

    List<Integer> selectCheckRoomType();
}