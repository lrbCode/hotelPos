package com.example.springbootdemo.module.checkin.domain;

public class Chart1 {
    private String name;
    private float data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getData() {
        return data;
    }

    public void setData(float data) {
        this.data = data;
    }
}
