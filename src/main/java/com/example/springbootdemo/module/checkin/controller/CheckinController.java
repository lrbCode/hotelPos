package com.example.springbootdemo.module.checkin.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.checkin.domain.Chart1;
import com.example.springbootdemo.module.checkin.domain.ChartDomain;
import com.example.springbootdemo.module.checkin.domain.Checkin;
import com.example.springbootdemo.module.checkin.service.ICheckinService;
import com.example.springbootdemo.module.common.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 订单\入住登记 信息操作处理
 * 
 * @author scau_imis43
 * @date 2019-05-07
 */
@Controller
@RequestMapping("/checkin")
public class CheckinController extends BaseController
{
    private String prefix = "module/checkin";
	
	@Autowired
	private ICheckinService checkinService;


	/**
	 * 折线图图表接口
	 * @return
	 */
	@RequestMapping("/highChart1")
	@ResponseBody
	public List<Chart1> highChart(ChartDomain chartDomain){

		List<Chart1> res=checkinService.getChart(chartDomain);
		return res;
	}


	@RequestMapping("/checkinManage")
	public String getIndex10() {
		return "checkin/checkinManage";
	}

	@RequestMapping("/checkinAdd")
	public String getIndex11() {
		return "checkin/checkinAdd";
	}

    @RequestMapping("/checkinEdit")
    public String getIndex12() {
        return "checkin/checkinEdit";
    }

    @RequestMapping("/checkinPay")
    public String getIndex13() {
        return "checkin/checkinPay";
    }

	@GetMapping("/list")
	@ResponseBody
	public ServerResponse list(Checkin checkin) {
		List<Checkin> list = checkinService.selectCheckinList(checkin);
		return ServerResponse.createBySuccess(list,list.size());
	}

	/**
	 * 新增保存客房当前详情,具体到房间号，一间房可以住多个客户
	 */
	@PostMapping("/addSave")
	@ResponseBody
	public ServerResponse addSave(@RequestBody Checkin checkin) {
		int res = checkinService.insertCheckin(checkin);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}


	@RequestMapping("/getRoomDetail")
	@ResponseBody
	public Map<String, List<Integer>> getRoomDetail() {
		Map<String, List<Integer>> res = checkinService.getRoomDetail();
		return  res;
	}
	/**
	 * 修改保存客房当前详情,具体到房间号，一间房可以住多个客户
	 */

	@PostMapping("/edit")
	@ResponseBody
	public ServerResponse editSave(Checkin checkin) {
		int res=checkinService.updateCheckin(checkin);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}
	@PostMapping("/checkOut")
	@ResponseBody
	public ServerResponse checkOut(String ids) {
		Checkin in =new Checkin();
		in.setLiveId(Integer.valueOf(ids));
		in.setDelFlag(1);
		int res = checkinService.updateCheckin(in);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}
	/**
	 * 删除客房当前详情,具体到房间号，一间房可以住多个客户
	 */

	@PostMapping("/remove")
	@ResponseBody
	public ServerResponse remove(String ids) {
		int res=checkinService.deleteCheckinByIds(ids);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}

}
