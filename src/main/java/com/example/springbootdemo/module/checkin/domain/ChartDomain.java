package com.example.springbootdemo.module.checkin.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChartDomain {
    /** 客户ID(自增) */
    private Integer rtid;
    /** 当前入住日期(天),钟点房精确到小时 */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date start;
    /** 当前退房日期(天),钟点房精确到小时 */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date end;

    public Integer getRtid() {
        return rtid;
    }

    public void setRtid(Integer rtid) {
        this.rtid = rtid;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }


}
