package com.example.springbootdemo.module.checkin.service;

import com.example.springbootdemo.module.checkin.domain.Chart1;
import com.example.springbootdemo.module.checkin.domain.ChartDomain;
import com.example.springbootdemo.module.checkin.domain.Checkin;
import java.util.List;
import java.util.Map;

/**
 * 订单\入住登记 服务层
 * 
 * @author scau_imis43
 * @date 2019-05-07
 */
public interface ICheckinService 
{
	/**
     * 查询订单\入住登记信息
     * 
     * @param liveId 订单\入住登记ID
     * @return 订单\入住登记信息
     */
	public Checkin selectCheckinById(Integer liveId);
	
	/**
     * 查询订单\入住登记列表
     * 
     * @param checkin 订单\入住登记信息
     * @return 订单\入住登记集合
     */
	public List<Checkin> selectCheckinList(Checkin checkin);
	
	/**
     * 新增订单\入住登记
     * 
     * @param checkin 订单\入住登记信息
     * @return 结果
     */
	public int insertCheckin(Checkin checkin);
	
	/**
     * 修改订单\入住登记
     * 
     * @param checkin 订单\入住登记信息
     * @return 结果
     */
	public int updateCheckin(Checkin checkin);
		
	/**
     * 删除订单\入住登记信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteCheckinByIds(String ids);

	Map<String, List<Integer>> getRoomDetail();

    List<Chart1> getChart(ChartDomain chartDomain);
}
