package com.example.springbootdemo.module.reserveType.service;

import com.example.springbootdemo.module.reserveType.domain.ReserveType;
import java.util.List;

/**
 * 预定—类型中间 服务层
 * 
 * @author scau_imis43
 * @date 2019-05-06
 */
public interface IReserveTypeService 
{
	/**
     * 查询预定—类型中间信息
     * 
     * @param phone 预定—类型中间ID
     * @return 预定—类型中间信息
     */
	public ReserveType selectReserveTypeById(String phone);
	
	/**
     * 查询预定—类型中间列表
     * 
     * @param reserveType 预定—类型中间信息
     * @return 预定—类型中间集合
     */
	public List<ReserveType> selectReserveTypeList(ReserveType reserveType);
	
	/**
     * 新增预定—类型中间
     * 
     * @param reserveType 预定—类型中间信息
     * @return 结果
     */
	public int insertReserveType(ReserveType reserveType);
	
	/**
     * 修改预定—类型中间
     * 
     * @param reserveType 预定—类型中间信息
     * @return 结果
     */
	public int updateReserveType(ReserveType reserveType);
		
	/**
     * 删除预定—类型中间信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteReserveTypeByIds(String ids);
	
}
