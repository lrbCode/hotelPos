package com.example.springbootdemo.module.reserveType.controller;

import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.reserveType.service.IReserveTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 预定—类型中间 信息操作处理
 * 
 * @author scau_imis43
 * @date 2019-05-06
 */
@Controller
@RequestMapping("/module/reserveType")
public class ReserveTypeController extends BaseController
{
    private String prefix = "module/reserveType";
	
	@Autowired
	private IReserveTypeService reserveTypeService;
	
	@RequiresPermissions("module:reserveType:view")
	@GetMapping()
	public String reserveType()
	{
	    return prefix + "/reserveType";
	}
	
	/**
	 * 查询预定—类型中间列表
	 *//*
	@RequiresPermissions("module:reserveType:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(ReserveType reserveType)
	{
		startPage();
        List<ReserveType> list = reserveTypeService.selectReserveTypeList(reserveType);
		return getDataTable(list);
	}
	
	
	*//**
	 * 导出预定—类型中间列表
	 *//*
	@RequiresPermissions("module:reserveType:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ReserveType reserveType)
    {
    	List<ReserveType> list = reserveTypeService.selectReserveTypeList(reserveType);
        ExcelUtil<ReserveType> util = new ExcelUtil<ReserveType>(ReserveType.class);
        return util.exportExcel(list, "reserveType");
    }
	
	*//**
	 * 新增预定—类型中间
	 *//*
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	*//**
	 * 新增保存预定—类型中间
	 *//*
	@RequiresPermissions("module:reserveType:add")
	@Log(title = "预定—类型中间", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(ReserveType reserveType)
	{		
		return toAjax(reserveTypeService.insertReserveType(reserveType));
	}

	*//**
	 * 修改预定—类型中间
	 *//*
	@GetMapping("/edit/{phone}")
	public String edit(@PathVariable("phone") String phone, ModelMap mmap)
	{
		ReserveType reserveType = reserveTypeService.selectReserveTypeById(phone);
		mmap.put("reserveType", reserveType);
	    return prefix + "/edit";
	}
	
	*//**
	 * 修改保存预定—类型中间
	 *//*
	@RequiresPermissions("module:reserveType:edit")
	@Log(title = "预定—类型中间", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(ReserveType reserveType)
	{		
		return toAjax(reserveTypeService.updateReserveType(reserveType));
	}
	
	*//**
	 * 删除预定—类型中间
	 *//*
	@RequiresPermissions("module:reserveType:remove")
	@Log(title = "预定—类型中间", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(reserveTypeService.deleteReserveTypeByIds(ids));
	}*/
	
}
