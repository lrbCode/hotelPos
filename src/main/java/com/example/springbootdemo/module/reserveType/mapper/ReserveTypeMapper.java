package com.example.springbootdemo.module.reserveType.mapper;

import com.example.springbootdemo.module.reserveType.domain.ReserveType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 预定—类型中间 数据层
 * 
 * @author scau_imis43
 * @date 2019-05-06
 */
public interface ReserveTypeMapper 
{
	/**
     * 查询预定—类型中间信息
     * 
     * @param phone 预定—类型中间ID
     * @return 预定—类型中间信息
     */
	public ReserveType selectReserveTypeById(String phone);
	
	/**
     * 查询预定—类型中间列表
     * 
     * @param reserveType 预定—类型中间信息
     * @return 预定—类型中间集合
     */
	public List<ReserveType> selectReserveTypeList(ReserveType reserveType);
	
	/**
     * 新增预定—类型中间
     * 
     * @param reserveType 预定—类型中间信息
     * @return 结果
     */
	public int insertReserveType(ReserveType reserveType);
	
	/**
     * 修改预定—类型中间
     * 
     * @param reserveType 预定—类型中间信息
     * @return 结果
     */
	public int updateReserveType(ReserveType reserveType);
	
	/**
     * 删除预定—类型中间
     * 
     * @param phone 预定—类型中间ID
     * @return 结果
     */
	public int deleteReserveTypeById(String phone);
	
	/**
     * 批量删除预定—类型中间
     * 
     * @param phones 需要删除的数据ID
     * @return 结果
     */
	public int deleteReserveTypeByIds(String[] phones);

	int selectByRtid(ReserveType t);

	int selectEmptyRoom(ReserveType t);

    int selectOrderNum(@Param("phoneTemp") String phoneTemp,@Param("rtid") String rtid);
}