package com.example.springbootdemo.module.reserveType.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.reserveType.domain.ReserveType;
import com.example.springbootdemo.module.reserveType.mapper.ReserveTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 预定—类型中间 服务层实现
 * 
 * @author scau_imis43
 * @date 2019-05-06
 */
@Service
public class ReserveTypeServiceImpl implements IReserveTypeService 
{
	@Autowired
	private ReserveTypeMapper reserveTypeMapper;

	/**
     * 查询预定—类型中间信息
     * 
     * @param phone 预定—类型中间ID
     * @return 预定—类型中间信息
     */
    @Override
	public ReserveType selectReserveTypeById(String phone)
	{
	    return reserveTypeMapper.selectReserveTypeById(phone);
	}
	
	/**
     * 查询预定—类型中间列表
     * 
     * @param reserveType 预定—类型中间信息
     * @return 预定—类型中间集合
     */
	@Override
	public List<ReserveType> selectReserveTypeList(ReserveType reserveType)
	{
	    return reserveTypeMapper.selectReserveTypeList(reserveType);
	}
	
    /**
     * 新增预定—类型中间
     * 
     * @param reserveType 预定—类型中间信息
     * @return 结果
     */
	@Override
	public int insertReserveType(ReserveType reserveType)
	{
	    return reserveTypeMapper.insertReserveType(reserveType);
	}
	
	/**
     * 修改预定—类型中间
     * 
     * @param reserveType 预定—类型中间信息
     * @return 结果
     */
	@Override
	public int updateReserveType(ReserveType reserveType)
	{
	    return reserveTypeMapper.updateReserveType(reserveType);
	}

	/**
     * 删除预定—类型中间对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteReserveTypeByIds(String ids)
	{
		return reserveTypeMapper.deleteReserveTypeByIds(Convert.toStrArray(ids));
	}
	
}
