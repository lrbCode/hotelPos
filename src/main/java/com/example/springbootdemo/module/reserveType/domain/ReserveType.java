package com.example.springbootdemo.module.reserveType.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 预定—类型中间表 reserve_type
 * 
 * @author scau_imis43
 * @date 2019-05-06
 */
public class ReserveType
{
	private static final long serialVersionUID = 1L;
	
	/** 手机号 */
	private String phone;
	/** 房间类型外键 */
	private Integer rtid;
	/** 预定数量 */
	private Integer orderNum;


	private String roomType;

	public void setPhone(String phone) 
	{
		this.phone = phone;
	}

	public String getPhone() 
	{
		return phone;
	}
	public void setRtid(Integer rtid) 
	{
		this.rtid = rtid;
	}

	public Integer getRtid() 
	{
		return rtid;
	}
	public void setOrderNum(Integer orderNum) 
	{
		this.orderNum = orderNum;
	}

	public Integer getOrderNum() 
	{
		return orderNum;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("phone", getPhone())
            .append("rtid", getRtid())
            .append("orderNum", getOrderNum())
            .toString();
    }

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
}
