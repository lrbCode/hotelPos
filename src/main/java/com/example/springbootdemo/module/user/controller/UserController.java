package com.example.springbootdemo.module.user.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.user.domain.User;
import com.example.springbootdemo.module.user.service.IUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 系统用户 信息操作处理
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
@Controller
@RequestMapping("/system")
public class UserController extends BaseController
{
    private String prefix = "user/";
	
	@Autowired
	private IUserService userService;

	
	//@RequiresPermissions("system:user:view")
    @RequiresPermissions("system:user:view")
	@GetMapping()
	public String user()
	{
	    return "user/userManage";
	}

	@RequestMapping("/editRole")
	public String editRole()
	{
		return "user/editRole";
	}
	
	/**
	 * 查询系统用户列表
	 */
	@RequiresPermissions("system:user:list")
	@RequestMapping("/list")
	@ResponseBody
	public ServerResponse list(User user)
	{

        List<User> list = userService.selectUserList(user);
		return ServerResponse.createBySuccess(list,list.size());
	}
	


	
	/**
	 * 新增保存系统用户
	 */
	@RequiresPermissions("system:user:add")
	@RequestMapping("/add")
	@ResponseBody
	public ServerResponse addSave(User user)
	{
		int res =userService.insertUser(user);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();

	}


	@RequestMapping("/checkRole")
	@ResponseBody
	public ServerResponse checkRole(Integer userId)
	{
		List<Integer> res=userService.selectRole(userId);
		return ServerResponse.createBySuccess(res) ;

	}


	/**
	 * 修改保存系统用户
	 */
	@RequiresPermissions("system:user:edit")
	@RequestMapping("/edit")
	@ResponseBody
	public ServerResponse editSave(User user)
	{
		int res =userService.updateUser(user);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}
	
	/**
	 * 删除系统用户
	 */
	@RequiresPermissions("system:user:remove")
	@RequestMapping( "/remove")
	@ResponseBody
	public ServerResponse remove(String ids)
	{
		int res =userService.deleteUserByIds(ids);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}
	
}
