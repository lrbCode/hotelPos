package com.example.springbootdemo.module.user.service;

import com.example.springbootdemo.module.user.domain.User;
import java.util.List;

/**
 * 系统用户 服务层
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
public interface IUserService 
{
	/**
     * 查询系统用户信息
     * 
     * @param userId 系统用户ID
     * @return 系统用户信息
     */
	public User selectUserById(Integer userId);
	
	/**
     * 查询系统用户列表
     * 
     * @param user 系统用户信息
     * @return 系统用户集合
     */
	public List<User> selectUserList(User user);
	
	/**
     * 新增系统用户
     * 
     * @param user 系统用户信息
     * @return 结果
     */
	public int insertUser(User user);
	
	/**
     * 修改系统用户
     * 
     * @param user 系统用户信息
     * @return 结果
     */
	public int updateUser(User user);
		
	/**
     * 删除系统用户信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteUserByIds(String ids);

	User findPassByLoginName(String loginName);

    boolean checkAdmin(Integer userId);

    List<Integer> selectRole(Integer userId);
}
