package com.example.springbootdemo.module.user.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.user.domain.User;
import com.example.springbootdemo.module.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统用户 服务层实现
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
@Service
public class UserServiceImpl implements IUserService 
{
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	@Autowired
	private UserMapper userMapper;

	/**
     * 查询系统用户信息
     * 
     * @param userId 系统用户ID
     * @return 系统用户信息
     */
    @Override
	public User selectUserById(Integer userId)
	{
	    return userMapper.selectUserById(userId);
	}
	
	/**
     * 查询系统用户列表
     * 
     * @param user 系统用户信息
     * @return 系统用户集合
     */
	@Override
	public List<User> selectUserList(User user)
	{
	    return userMapper.selectUserList(user);
	}
	
    /**
     * 新增系统用户
     * 
     * @param user 系统用户信息
     * @return 结果
     */
	@Override
	public int insertUser(User user)
	{
	    return userMapper.insertUser(user);
	}
	
	/**
     * 修改系统用户
     * 
     * @param user 系统用户信息
     * @return 结果
     */
	@Override
	public int updateUser(User user)
	{
	    return userMapper.updateUser(user);
	}

	/**
     * 删除系统用户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteUserByIds(String ids)
	{
		return userMapper.deleteUserByIds(Convert.toStrArray(ids));
	}

	@Override
	public User findPassByLoginName(String loginName) {
		return userMapper.selectPassByLoginName(loginName);
	}

	@Override
	public boolean checkAdmin(Integer userId) {
		return userMapper.checkAdmin(userId)>0;
	}

	@Override
	public List<Integer> selectRole(Integer userId) {
		List<Integer> roleId=userMapper.checkRole(userId);
		for (int i = 0; i <roleId.size() ; i++) {
			System.out.println(roleId.get(i));
		}


		return roleId;
	}

}
