package com.example.springbootdemo.module.user.domain;

import com.example.springbootdemo.module.role.domain.Role;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;

import java.util.List;

/**
 * 系统用户表 pos_user
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
public class User
{
	private static final long serialVersionUID = 1L;
	
	/** 用户ID */
	private Integer userId;
	/** 登录账号 */
	private String loginName;
	/** 密码 */
	private String password;
	/** 盐加密 */
	private String salt;
	/** 用户昵称 */
	private String userName;
	/** 用户类型（默认 0管理员用户 1前台用户） */
	private String userType;
	/** 手机号码 */
	private String phonenumber;
	/** 用户性别（0男 1女 2未知） */
	private String sex;
	/** 帐号状态（0正常 1停用） */
	private String status;
	/** 备注 */
	private String remark;
	/** 删除标志（0代表存在 2代表删除） */
	private String delFlag;


	/** 角色集合 */
	private List<Role> roles;

	/** 角色组 */
	private Integer[] roleIds;


	/**
	 * 生成随机盐
	 */
	public void randomSalt()
	{
		// 一个Byte占两个字节，此处生成的3字节，字符串长度为6
		SecureRandomNumberGenerator secureRandom = new SecureRandomNumberGenerator();
		String hex = secureRandom.nextBytes(3).toHex();
		setSalt(hex);
	}



	public void setUserId(Integer userId) 
	{
		this.userId = userId;
	}

	public Integer getUserId() 
	{
		return userId;
	}
	public void setLoginName(String loginName) 
	{
		this.loginName = loginName;
	}

	public String getLoginName() 
	{
		return loginName;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}

	public String getPassword() 
	{
		return password;
	}
	public void setSalt(String salt) 
	{
		this.salt = salt;
	}

	public String getSalt() 
	{
		return salt;
	}
	public void setUserName(String userName) 
	{
		this.userName = userName;
	}

	public String getUserName() 
	{
		return userName;
	}
	public void setUserType(String userType) 
	{
		this.userType = userType;
	}

	public String getUserType() 
	{
		return userType;
	}
	public void setPhonenumber(String phonenumber) 
	{
		this.phonenumber = phonenumber;
	}

	public String getPhonenumber() 
	{
		return phonenumber;
	}
	public void setSex(String sex) 
	{
		this.sex = sex;
	}

	public String getSex() 
	{
		return sex;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}
	public void setDelFlag(String delFlag) 
	{
		this.delFlag = delFlag;
	}

	public String getDelFlag() 
	{
		return delFlag;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("loginName", getLoginName())
            .append("password", getPassword())
            .append("salt", getSalt())
            .append("userName", getUserName())
            .append("userType", getUserType())
            .append("phonenumber", getPhonenumber())
            .append("sex", getSex())
            .append("status", getStatus())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
