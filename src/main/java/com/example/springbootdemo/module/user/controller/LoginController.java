package com.example.springbootdemo.module.user.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @PostMapping("/doLogin")
    public String login(String username, String password) {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try {
            subject.login(token);
       //     token.setRememberMe(true);
        }catch (AuthenticationException e) {
            token.clear();
            throw e;
        }


        return "0";
    }


    @RequestMapping("/logout")
    public String logout() {
        System.out.println("退出");
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.logout();
        return "admin/login";
    }



}
