package com.example.springbootdemo.module.userRole.user;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.framework.utils.StringUtils;
import com.example.springbootdemo.module.userRole.domain.UserRole;
import com.example.springbootdemo.module.userRole.mapper.UserRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户和角色关联232323 服务层实现
 *
 * @author scau_imis43
 * @date 2019-06-26
 */
@Service
public class UserRoleServiceImpl implements IUserRoleService {
    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     * 查询用户和角色关联232323信息
     *
     * @param userId 用户和角色关联232323ID
     * @return 用户和角色关联232323信息
     */
    @Override
    public UserRole selectUserRoleById(Integer userId) {
        return userRoleMapper.selectUserRoleById(userId);
    }

    /**
     * 查询用户和角色关联232323列表
     *
     * @param userRole 用户和角色关联232323信息
     * @return 用户和角色关联232323集合
     */
    @Override
    public List<UserRole> selectUserRoleList(UserRole userRole) {
        return userRoleMapper.selectUserRoleList(userRole);
    }

    /**
     * 新增用户和角色关联232323
     *
     * @param userRole 用户和角色关联232323信息
     * @return 结果
     */
    @Override
    public int insertUserRole(UserRole userRole)

    {
        int[] roles = userRole.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色管理
            List<UserRole> list = new ArrayList<UserRole>();
            for (int roleId : userRole.getRoleIds()) {
                UserRole ur = new UserRole();
                ur.setUserId(userRole.getUserId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0) {
                userRoleMapper.batchUserRole(list);
            }
        }
        return 0;
    }

    /**
     * 修改用户和角色关联232323
     *
     * @param userRole 用户和角色关联232323信息
     * @return 结果
     */
    @Override
    public int updateUserRole(UserRole userRole) { // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userRole.getUserId());
        // 新增用户与角色管理
        insertUserRole(userRole);
        return 1;
    }

    /**
     * 删除用户和角色关联232323对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUserRoleByIds(String ids) {
        return userRoleMapper.deleteUserRoleByIds(Convert.toStrArray(ids));
    }

}
