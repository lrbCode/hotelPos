package com.example.springbootdemo.module.userRole.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.userRole.domain.UserRole;
import com.example.springbootdemo.module.userRole.user.IUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户和角色关联232323 信息操作处理
 * 
 * @author scau_imis43
 * @date 2019-06-26
 */
@Controller
@RequestMapping("/userRole")
public class UserRoleController extends BaseController
{
    private String prefix = "module/userRole";
	
	@Autowired
	private IUserRoleService userRoleService;
	

	@GetMapping()
	public String userRole()
	{
	    return prefix + "/userRole";
	}
	
	/**
	 * 查询用户和角色关联232323列表
	 */

	@PostMapping("/list")
	@ResponseBody
	public ServerResponse list(UserRole userRole)
	{

        List<UserRole> list = userRoleService.selectUserRoleList(userRole);
		return ServerResponse.createBySuccess(list,list.size());
	}
	

	/**
	 * 修改保存用户和角色关联232323
	 */


	@RequestMapping("/edit")
	@ResponseBody
	public ServerResponse editSave(@RequestBody UserRole userRole)
	{
		System.out.println("==============="+userRole.getUserId());
		System.out.println("==============="+userRole.getRoleIds());
		int res=userRoleService.updateUserRole(userRole);
		return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
	}
	

}
