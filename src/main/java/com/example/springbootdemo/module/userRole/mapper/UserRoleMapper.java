package com.example.springbootdemo.module.userRole.mapper;

import com.example.springbootdemo.module.userRole.domain.UserRole;
import java.util.List;	

/**
 * 用户和角色关联232323 数据层
 * 
 * @author scau_imis43
 * @date 2019-06-26
 */
public interface UserRoleMapper 
{
	/**
     * 查询用户和角色关联232323信息
     * 
     * @param userId 用户和角色关联232323ID
     * @return 用户和角色关联232323信息
     */
	public UserRole selectUserRoleById(Integer userId);
	
	/**
     * 查询用户和角色关联232323列表
     * 
     * @param userRole 用户和角色关联232323信息
     * @return 用户和角色关联232323集合
     */
	public List<UserRole> selectUserRoleList(UserRole userRole);
	
	/**
     * 新增用户和角色关联232323
     * 
     * @param userRole 用户和角色关联232323信息
     * @return 结果
     */
	public int insertUserRole(UserRole userRole);
	
	/**
     * 修改用户和角色关联232323
     * 
     * @param userRole 用户和角色关联232323信息
     * @return 结果
     */
	public int updateUserRole(UserRole userRole);
	
	/**
     * 删除用户和角色关联232323
     * 
     * @param userId 用户和角色关联232323ID
     * @return 结果
     */
	public int deleteUserRoleById(Integer userId);
	
	/**
     * 批量删除用户和角色关联232323
     * 
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteUserRoleByIds(String[] userIds);

	public int  deleteUserRoleByUserId(Integer userId);
	public int batchUserRole(List<UserRole> userRoleList);
}