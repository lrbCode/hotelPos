package com.example.springbootdemo.module.userRole.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户和角色关联232323表 pos_user_role
 * 
 * @author scau_imis43
 * @date 2019-06-26
 */
public class UserRole
{
	private static final long serialVersionUID = 1L;
	
	/** 用户ID */
	private Integer userId;
	/** 角色ID */
	private Integer roleId;

	/** 角色组 */
	private int[] roleIds;

	public int[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(int[] roleIds) {
		this.roleIds = roleIds;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public Integer getUserId() 
	{
		return userId;
	}
	public void setRoleId(Integer roleId) 
	{
		this.roleId = roleId;
	}

	public Integer getRoleId() 
	{
		return roleId;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("userId", getUserId())
            .append("roleId", getRoleId())
            .toString();
    }
}
