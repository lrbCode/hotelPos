package com.example.springbootdemo.module.price.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 房间价格表 room_price
 * 
 * @author scau_imis43
 * @date 2019-04-06
 */
public class Price
{
	private static final long serialVersionUID = 1L;
	
	/** 主键 */
	private Integer id;
	/** 房间类型外键 */
	private Integer rtid;
	/** 客房类型价格 */
	private Float price;
	/** 价格时间(入住那一天) */
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date time;
	/** 备注 */
	private String remark;

	public void setId(Integer id) 
	{
		this.id = id;
	}

	public Integer getId() 
	{
		return id;
	}
	public void setRtid(Integer rtid) 
	{
		this.rtid = rtid;
	}

	public Integer getRtid() 
	{
		return rtid;
	}
	public void setPrice(Float price) 
	{
		this.price = price;
	}

	public Float getPrice() 
	{
		return price;
	}
	public void setTime(Date time) 
	{
		this.time = time;
	}

	public Date getTime() 
	{
		return time;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("rtid", getRtid())
            .append("price", getPrice())
            .append("time", getTime())
            .append("remark", getRemark())
            .toString();
    }
}
