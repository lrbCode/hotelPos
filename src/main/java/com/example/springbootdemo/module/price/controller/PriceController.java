package com.example.springbootdemo.module.price.controller;

import com.example.springbootdemo.framework.common.ServerResponse;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.price.domain.PriceVo;
import com.example.springbootdemo.module.price.service.IPriceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 房间价格 信息操作处理
 *
 * @author scau_imis43
 * @date 2019-04-06
 */
@Controller
@RequestMapping("/roomPrice")
public class PriceController extends BaseController {
    private String prefix = "roomPrice/";

    @Autowired
    private IPriceService priceService;

    @RequiresPermissions("system:roomPrice:view")
    @GetMapping()
    public String price() {
        return prefix + "roomPrice";
    }

    @GetMapping("/roomPriceCalendar")
    public String priceCalendar() {
        return prefix + "roomPriceCalendar";
    }

    /**
     * 查询房间价格列表
     */

    @RequiresPermissions("system:roomPrice:list")
    @GetMapping("/list")
    @ResponseBody
    public ServerResponse list(PriceVo price) {
        List<PriceVo> list = priceService.selectPriceList(price);
        return ServerResponse.createBySuccess(list,list.size());
    }


    /*	*//**
     * 导出房间价格列表
     *//*
	@RequiresPermissions("module:price:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Price price)
    {
    	List<Price> list = priceService.selectPriceList(price);
        ExcelUtil<Price> util = new ExcelUtil<Price>(Price.class);
        return util.exportExcel(list, "price");
    }*/

    /*	*//**
     * 新增房间价格
     *//*
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	*/

    /**
     * 新增保存房间价格
     */
    @RequiresPermissions("system:roomPrice:add")
    @PostMapping("/addSave")
    @ResponseBody
    public ServerResponse addSave(PriceVo price) {
        int res = priceService.insertPrice(price);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();
    }

    /*	*//**
     * 修改房间价格
     *//*
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Integer id, ModelMap mmap)
	{
		Price price = priceService.selectPriceById(id);
		mmap.put("price", price);
	    return prefix + "/edit";
	}*/

    /**
     * 修改保存房间价格
     */
    @RequiresPermissions("system:roomPrice:edit")
    @PostMapping("/edit")
    @ResponseBody
    public ServerResponse editSave(PriceVo price) {
        int res = priceService.updatePrice(price);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();

    }

    /**
     * 删除房间价格
     */

    @PostMapping("/remove")
    @ResponseBody
    public ServerResponse remove(String ids) {
        int res = priceService.deletePriceByIds(ids);
        return res > 0 ? ServerResponse.createBySuccess() : ServerResponse.createByError();

    }

}
