package com.example.springbootdemo.module.price.mapper;

import com.example.springbootdemo.module.price.domain.PriceVo;

import java.util.List;

/**
 * 房间价格 数据层
 * 
 * @author scau_imis43
 * @date 2019-04-06
 */
public interface PriceMapper 
{
	/**
     * 查询房间价格信息
     * 
     * @param id 房间价格ID
     * @return 房间价格信息
     */
	public PriceVo selectPriceById(Integer id);
	
	/**
     * 查询房间价格列表
     * 
     * @param price 房间价格信息
     * @return 房间价格集合
     */
	public List<PriceVo> selectPriceList(PriceVo price);
	
	/**
     * 新增房间价格
     * 
     * @param price 房间价格信息
     * @return 结果
     */
	public int insertPrice(PriceVo price);
	
	/**
     * 修改房间价格
     * 
     * @param price 房间价格信息
     * @return 结果
     */
	public int updatePrice(PriceVo price);
	
	/**
     * 删除房间价格
     * 
     * @param id 房间价格ID
     * @return 结果
     */
	public int deletePriceById(Integer id);
	
	/**
     * 批量删除房间价格
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deletePriceByIds(String[] ids);
	
}