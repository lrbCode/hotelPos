package com.example.springbootdemo.module.price.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.module.price.domain.PriceVo;
import com.example.springbootdemo.module.price.mapper.PriceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 房间价格 服务层实现
 * 
 * @author scau_imis43
 * @date 2019-04-06
 */
@Service
public class PriceServiceImpl implements IPriceService 
{
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	@Autowired
	private PriceMapper priceMapper;

	/**
     * 查询房间价格信息
     * 
     * @param id 房间价格ID
     * @return 房间价格信息
     */
    @Override
	public PriceVo selectPriceById(Integer id)
	{
	    return priceMapper.selectPriceById(id);
	}
	
	/**
     * 查询房间价格列表
     * 
     * @param price 房间价格信息
     * @return 房间价格集合
     */
	@Override
	public List<PriceVo> selectPriceList(PriceVo price)
	{
	    return priceMapper.selectPriceList(price);
	}
	
    /**
     * 新增房间价格
     * 
     * @param price 房间价格信息
     * @return 结果
     */
	@Override
	public int insertPrice(PriceVo price)
	{
	    return priceMapper.insertPrice(price);
	}
	
	/**
     * 修改房间价格
     * 
     * @param price 房间价格信息
     * @return 结果
     */
	@Override
	public int updatePrice(PriceVo price)
	{
	    return priceMapper.updatePrice(price);
	}

	/**
     * 删除房间价格对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deletePriceByIds(String ids)
	{
		return priceMapper.deletePriceByIds(Convert.toStrArray(ids));
	}
	
}
