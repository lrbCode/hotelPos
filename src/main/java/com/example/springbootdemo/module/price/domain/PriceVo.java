package com.example.springbootdemo.module.price.domain;

public class PriceVo extends Price{
    private String roomType;

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }
}
