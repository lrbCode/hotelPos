package com.example.springbootdemo.module.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CommonController {

    private String prefix = "admin/";

    @GetMapping("/unauth")
    public String unauth()
    {
        return "error/unauth";
    }
    @RequestMapping("/admin/login")
    public String getIndex2(){
        return prefix+"login";
    }



    @RequestMapping("/admin/index")
    public String getIndex(){
        return prefix+"index";
    }












    @RequestMapping("/admin/member")
    public String getIndex3(){
        return prefix+"member-list";
    }
    @RequestMapping("/admin/welcome")
    public String getIndex4(){
        return prefix+"welcome";
    }
    @RequestMapping("/admin/member1")
    public String getIndex5(){
        return prefix+"member-list1";
    }


    @RequestMapping("/admin/roomEdit")
    public String getIndex9(){
        return prefix+"roomEdit";
    }
    @RequestMapping("/admin/roominfo.json")
    public String getIndex10(){
        return prefix+"roominfo.json";
    }
    @RequestMapping("/admin/member2")
    public String getIndex11(){
        return prefix+"member-add";
    }
    @RequestMapping("/admin/orderadd")
    public String getIndex12(){
        return prefix+"order-add";
    }
    @RequestMapping("/admin/roomTypeInfo")
    public String getIndex13(){
        return prefix+"roomTypeInfo";
    }
    @RequestMapping("/admin/roomNumManage")
    public String getIndex14(){ return prefix+"roomNumManage";}
    @RequestMapping("/admin/package")
    public String getIndex15(){ return prefix+"package.json";}
    @RequestMapping("/admin/roomCheck")
    public String getIndex16(){ return prefix+"roomCheck";}





}
