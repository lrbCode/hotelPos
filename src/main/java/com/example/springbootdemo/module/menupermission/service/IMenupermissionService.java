package com.example.springbootdemo.module.menupermission.service;

import com.example.springbootdemo.module.menupermission.domain.Menupermission;
import com.example.springbootdemo.module.role.domain.Role;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 菜单权限 服务层
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
public interface IMenupermissionService 
{
	/**
     * 查询菜单权限信息
     * 
     * @param menuId 菜单权限ID
     * @return 菜单权限信息
     */
	public Menupermission selectMenupermissionById(Integer menuId);
	public Set<String> selectPermsByUserId(Integer userId);
	/**
     * 查询菜单权限列表
     * 
     * @param menupermission 菜单权限信息
     * @return 菜单权限集合
     */
	public List<Menupermission> selectMenupermissionList(Menupermission menupermission);
	
	/**
     * 新增菜单权限
     * 
     * @param menupermission 菜单权限信息
     * @return 结果
     */
	public int insertMenupermission(Menupermission menupermission);
	
	/**
     * 修改菜单权限
     * 
     * @param menupermission 菜单权限信息
     * @return 结果
     */
	public int updateMenupermission(Menupermission menupermission);
		
	/**
     * 删除菜单权限信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteMenupermissionByIds(String ids);
	public List<Map<String, Object>> roleMenuTreeData(Role role);
}
