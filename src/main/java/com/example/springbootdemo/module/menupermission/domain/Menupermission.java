package com.example.springbootdemo.module.menupermission.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单权限表 pos_menupermission
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
public class Menupermission
{
	private static final long serialVersionUID = 1L;
	
	/** 菜单ID */
	private Integer menuId;
	/** 菜单名称 */
	private String menuName;
	/** 父菜单ID */
	private Integer parentId;
	/** 请求地址 */
	private String url;
	/** 菜单类型（M目录 C菜单 F按钮） */
	private String menuType;
	/** 菜单状态（0显示 1隐藏） */
	private String visible;
	/** 权限标识 */
	private String perms;
	/** 备注 */
	private String remark;
	/** 子菜单 */
	private List<Menupermission> children = new ArrayList<>();

	public void setMenuId(Integer menuId) 
	{
		this.menuId = menuId;
	}

	public Integer getMenuId() 
	{
		return menuId;
	}
	public void setMenuName(String menuName) 
	{
		this.menuName = menuName;
	}

	public String getMenuName() 
	{
		return menuName;
	}
	public void setParentId(Integer parentId) 
	{
		this.parentId = parentId;
	}

	public Integer getParentId() 
	{
		return parentId;
	}
	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getUrl() 
	{
		return url;
	}
	public void setMenuType(String menuType) 
	{
		this.menuType = menuType;
	}

	public String getMenuType() 
	{
		return menuType;
	}
	public void setVisible(String visible) 
	{
		this.visible = visible;
	}

	public String getVisible() 
	{
		return visible;
	}
	public void setPerms(String perms) 
	{
		this.perms = perms;
	}

	public String getPerms() 
	{
		return perms;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("menuId", getMenuId())
            .append("menuName", getMenuName())
            .append("parentId", getParentId())
            .append("url", getUrl())
            .append("menuType", getMenuType())
            .append("visible", getVisible())
            .append("perms", getPerms())
            .append("remark", getRemark())
            .toString();
    }

	public List<Menupermission> getChildren() {
		return children;
	}

	public void setChildren(List<Menupermission> children) {
		this.children = children;
	}
}
