package com.example.springbootdemo.module.menupermission.mapper;

import com.example.springbootdemo.module.menupermission.domain.Menupermission;
import java.util.List;	

/**
 * 菜单权限 数据层
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
public interface MenupermissionMapper 
{
	/**
     * 查询菜单权限信息
     * 
     * @param menuId 菜单权限ID
     * @return 菜单权限信息
     */
	public Menupermission selectMenupermissionById(Integer menuId);
	
	/**
     * 查询菜单权限列表
     * 
     * @param menupermission 菜单权限信息
     * @return 菜单权限集合
     */
	public List<Menupermission> selectMenupermissionList(Menupermission menupermission);
	
	/**
     * 新增菜单权限
     * 
     * @param menupermission 菜单权限信息
     * @return 结果
     */
	public int insertMenupermission(Menupermission menupermission);
	
	/**
     * 修改菜单权限
     * 
     * @param menupermission 菜单权限信息
     * @return 结果
     */
	public int updateMenupermission(Menupermission menupermission);
	
	/**
     * 删除菜单权限
     * 
     * @param menuId 菜单权限ID
     * @return 结果
     */
	public int deleteMenupermissionById(Integer menuId);
	
	/**
     * 批量删除菜单权限
     * 
     * @param menuIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteMenupermissionByIds(String[] menuIds);

    List<String> selectPermsByUserId(Integer userId);

    List<Menupermission> selectMenuAll();

	List<String> selectMenuTree(Integer roleId);
}