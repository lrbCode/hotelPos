package com.example.springbootdemo.module.menupermission.service;

import com.example.springbootdemo.framework.utils.Convert;
import com.example.springbootdemo.framework.utils.StringUtils;
import com.example.springbootdemo.module.menupermission.domain.Menupermission;
import com.example.springbootdemo.module.menupermission.mapper.MenupermissionMapper;
import com.example.springbootdemo.module.role.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 菜单权限 服务层实现
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
@Service
public class MenupermissionServiceImpl implements IMenupermissionService 
{
	@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
	@Autowired
	private MenupermissionMapper menupermissionMapper;

	/**
	 * 根据角色ID查询菜单
	 *
	 * @param role 角色对象
	 * @return 菜单列表
	 */
	@Override
	public List<Map<String, Object>> roleMenuTreeData(Role role)
	{
		Integer roleId = role.getRoleId();
		List<Map<String, Object>> trees = new ArrayList<>();
		List<Menupermission> menuList = menupermissionMapper.selectMenuAll();
		if (StringUtils.isNotNull(roleId))
		{
			List<String> roleMenuList = menupermissionMapper.selectMenuTree(roleId);
			trees = getTrees(menuList, true, roleMenuList, true);
		}
		else
		{
			trees = getTrees(menuList, false, null, true);
		}
		return trees;
	}

	/**
	 * 对象转菜单树
	 *
	 * @param menuList 菜单列表
	 * @param isCheck 是否需要选中
	 * @param roleMenuList 角色已存在菜单列表
	 * @param permsFlag 是否需要显示权限标识
	 * @return
	 */
	public List<Map<String, Object>> getTrees(List<Menupermission> menuList, boolean isCheck, List<String> roleMenuList,
											  boolean permsFlag)
	{
		List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
		for (Menupermission menu : menuList)
		{
			Map<String, Object> deptMap = new HashMap<String, Object>();
			deptMap.put("id", menu.getMenuId());
			deptMap.put("pId", menu.getParentId());
			deptMap.put("name", transMenuName(menu, roleMenuList, permsFlag));
			deptMap.put("title", menu.getMenuName());
			if (isCheck)
			{
				deptMap.put("checked", roleMenuList.contains(menu.getMenuId() + menu.getPerms()));
			}
			else
			{
				deptMap.put("checked", false);
			}
			trees.add(deptMap);
		}
		return trees;
	}

	public String transMenuName(Menupermission menu, List<String> roleMenuList, boolean permsFlag)
	{
		StringBuffer sb = new StringBuffer();
		sb.append(menu.getMenuName());
		if (permsFlag)
		{
			sb.append("<font color=\"#888\">&nbsp;&nbsp;&nbsp;" + menu.getPerms() + "</font>");
		}
		return sb.toString();
	}

















	/**
     * 查询菜单权限信息
     * 
     * @param menuId 菜单权限ID
     * @return 菜单权限信息
     */
    @Override
	public Menupermission selectMenupermissionById(Integer menuId)
	{
	    return menupermissionMapper.selectMenupermissionById(menuId);
	}

    @Override
    public Set<String> selectPermsByUserId(Integer userId) {
		List<String> perms = menupermissionMapper.selectPermsByUserId(userId);
		Set<String> permsSet = new HashSet<>();
		for (String perm : perms)
		{

			if (StringUtils.isNotEmpty(perm))
			{
				permsSet.addAll(Arrays.asList(perm.trim().split(",")));
			}
		}
		return permsSet;
    }

    /**
     * 查询菜单权限列表
     * 
     * @param menupermission 菜单权限信息
     * @return 菜单权限集合
     */
	@Override
	public List<Menupermission> selectMenupermissionList(Menupermission menupermission)
	{
	    return menupermissionMapper.selectMenupermissionList(menupermission);
	}
	
    /**
     * 新增菜单权限
     * 
     * @param menupermission 菜单权限信息
     * @return 结果
     */
	@Override
	public int insertMenupermission(Menupermission menupermission)
	{
	    return menupermissionMapper.insertMenupermission(menupermission);
	}
	
	/**
     * 修改菜单权限
     * 
     * @param menupermission 菜单权限信息
     * @return 结果
     */
	@Override
	public int updateMenupermission(Menupermission menupermission)
	{
	    return menupermissionMapper.updateMenupermission(menupermission);
	}

	/**
     * 删除菜单权限对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteMenupermissionByIds(String ids)
	{
		return menupermissionMapper.deleteMenupermissionByIds(Convert.toStrArray(ids));
	}
	
}
