package com.example.springbootdemo.module.menupermission.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.example.springbootdemo.module.common.BaseController;
import com.example.springbootdemo.module.menupermission.service.IMenupermissionService;
import com.example.springbootdemo.module.role.domain.Role;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 菜单权限 信息操作处理
 * 
 * @author scau_imis43
 * @date 2019-04-08
 */
@Controller
@RequestMapping("/menupermission")
public class MenupermissionController extends BaseController
{
    private String prefix = "menupermission/";
	
	@Autowired
	private IMenupermissionService menupermissionService;


	/**
	 * 加载角色菜单列表树
	 */
	@GetMapping("/roleMenuTreeData")
	@ResponseBody
	public List<Map<String, Object>> roleMenuTreeData(int roleId)
	{

	    Role role=new Role();
	    role.setRoleId(roleId);
		List<Map<String, Object>> tree = menupermissionService.roleMenuTreeData(role);
        System.out.println(JSONUtils.toJSONString(tree));
		return tree;
	}









	@RequiresPermissions("module:menupermission:view")
	@GetMapping()
	public String menupermission()
	{
	    return prefix + "/menupermission";
	}
	
	/**
	 * 查询菜单权限列表
	/* *//*
	@RequiresPermissions("module:menupermission:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Menupermission menupermission)
	{
		startPage();
        List<Menupermission> list = menupermissionService.selectMenupermissionList(menupermission);
		return getDataTable(list);
	}
	
	
	*//**
	 * 导出菜单权限列表
	 *//*
	@RequiresPermissions("module:menupermission:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Menupermission menupermission)
    {
    	List<Menupermission> list = menupermissionService.selectMenupermissionList(menupermission);
        ExcelUtil<Menupermission> util = new ExcelUtil<Menupermission>(Menupermission.class);
        return util.exportExcel(list, "menupermission");
    }
	
	*//**
	 * 新增菜单权限
	 *//*
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	*//**
	 * 新增保存菜单权限
	 *//*
	@RequiresPermissions("module:menupermission:add")
	@Log(title = "菜单权限", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Menupermission menupermission)
	{		
		return toAjax(menupermissionService.insertMenupermission(menupermission));
	}

	*//**
	 * 修改菜单权限
	 *//*
	@GetMapping("/edit/{menuId}")
	public String edit(@PathVariable("menuId") Integer menuId, ModelMap mmap)
	{
		Menupermission menupermission = menupermissionService.selectMenupermissionById(menuId);
		mmap.put("menupermission", menupermission);
	    return prefix + "/edit";
	}
	
	*//**
	 * 修改保存菜单权限
	 *//*
	@RequiresPermissions("module:menupermission:edit")
	@Log(title = "菜单权限", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Menupermission menupermission)
	{		
		return toAjax(menupermissionService.updateMenupermission(menupermission));
	}
	
	*//**
	 * 删除菜单权限
	 *//*
	@RequiresPermissions("module:menupermission:remove")
	@Log(title = "菜单权限", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(menupermissionService.deleteMenupermissionByIds(ids));
	}*/
	
}
