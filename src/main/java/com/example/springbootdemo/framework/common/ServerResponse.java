package com.example.springbootdemo.framework.common;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * Created by 1032019725 on 2017/10/17.
 */
/*@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)*/
//保证json序列化的时候，value为null的时候，key也会消失
public class ServerResponse<T> implements Serializable {
    private int status;
    private String msg="";
    private int total;
    private T data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    private ServerResponse(int status){
        this.status = status;
    }
    private ServerResponse(int status, T data,int total){
        this.status = status;
        this.data = data;
        this.total=total;
    }
    private ServerResponse(int status, T data){
        this.status = status;
        this.data = data;

    }
    private ServerResponse(int status, String msg, T data){
        this.status = status;
        this.msg = msg;
        this.data = data;
    }
    private ServerResponse(int status, String msg){
        this.status = status;
        this.msg = msg;
    }
    @JsonIgnore
//json序列化返回前端的时候，忽略该方法，不需要返回
    public boolean isSuccess() {
        return this.status == ResponseCode.SUCCESS.getCode();
    }
    public int getStatus(){
        return  status;
    }
    public String getMsg(){
        return  msg;
    }
    public T getData(){
        return data;
    }

    public static <T> ServerResponse<T> createBySuccess(){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode());
    }
    public static <T> ServerResponse<T> createBySuccessMessage(String msg){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg);
    }
    public static <T> ServerResponse<T> createBySuccess(T data){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),data);
    }
    public static <T> ServerResponse<T> createBySuccess(T data,int total){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),data,total);
    }




    public static <T> ServerResponse<T> createByError(){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
    }
    public static <T> ServerResponse<T> createByErrorMessage(String errorMessage){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),errorMessage);
    }
    public static <T> ServerResponse<T> createByErrorCodeMessage(int errorCode,String errorMessage){
        return new ServerResponse<T>(errorCode,errorMessage);

    }


}
