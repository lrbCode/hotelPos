package com.example.springbootdemo.framework.exception;

import com.example.springbootdemo.framework.common.ServerResponse;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionController {

    private static final Logger log = LoggerFactory.getLogger(ExceptionController.class);

/*    @ExceptionHandler(Exception.class)
    public String handlerException(Exception e) {
        log.error(e.getMessage());
        //return new ResponseEntity("系统发生异常，请联系管理员",HttpStatus.INTERNAL_SERVER_ERROR);
        return "admin/error.html";
    }*/

    /**
     * 权限校验失败
     */
    /*@ExceptionHandler(AuthorizationException.class)
    public String handleAuthorizationException(AuthorizationException e)
    {
        log.error("授权失败：",e.getMessage());
        return "error/unauth.html";
    }*/
    @ExceptionHandler(IncorrectCredentialsException.class)
    @ResponseBody
    public ServerResponse handlerException(IncorrectCredentialsException e) {
        log.error(e.getMessage());
        return ServerResponse.createByErrorMessage("密码错误");
    }

    @ExceptionHandler(DisabledAccountException.class)
    @ResponseBody
    public ServerResponse handlerException(DisabledAccountException e) {
        log.error(e.getMessage());
        return ServerResponse.createByErrorMessage("账号已停用");
    }

    @ExceptionHandler(UnknownAccountException.class)
    @ResponseBody
    public ServerResponse handlerException(UnknownAccountException e) {
        log.error(e.getMessage());
        return ServerResponse.createByErrorMessage("账号不存在");
    }
}
