package com.example.springbootdemo.framework.shrio.service;

import com.example.springbootdemo.module.menupermission.service.IMenupermissionService;
import com.example.springbootdemo.module.role.service.IRoleService;
import com.example.springbootdemo.module.user.domain.User;
import com.example.springbootdemo.module.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class ShiroService {

    @Autowired
    IUserService iUserService;
    @Autowired
    IRoleService iRoleService;
    @Autowired
    IMenupermissionService iMenupermissionService;

    public User findPassByLoginName(String loginName){
        return iUserService.findPassByLoginName(loginName);
    }

    public boolean checkAdmin(Integer userId) {
        return iUserService.checkAdmin(userId);
    }

    public Set<String> selectRoleKeys(Integer userId) {
        return iRoleService.selectRoleKeys(userId);
    }


    public Set<String> selectPermsByUserId(Integer userId) {
        return iMenupermissionService.selectPermsByUserId(userId);
    }
}
