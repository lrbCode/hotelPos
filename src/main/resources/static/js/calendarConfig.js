var config = {
    modules:{
        'price-calendar': {
            fullpath: '/js/price-calendar.js',
            type: 'js',
            requires: ['price-calendar-css']
        },
        'price-calendar-css': {
            fullpath: '/css/price-calendar.css',
            type: 'css'
        }
    }

}