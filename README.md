# hotelPos

#### 介绍
快捷酒店POS，组员：16信管4 第三组

#### 软件架构
系统需求JDK >= 1.8 MySQL >= 5.7 Maven >= 3.0

开发工具：IntelliJ IDEA

核心框架Spring Boot 2.0以上

持久层： Apache MyBatis 3.4  Alibaba Druid 1.1

视图层：Bootstrap 3.3  Thymeleaf 3.0  Jquery  Ajax

缓存框架： Ehcache 2.6或Guava cache

日志管理： SLF4J作为日志门面、Log4j作为主日志框架

安全框架: Apache Shiro 1.4以上


#### 使用说明

1. xxxx
2. xxxx
3. xxxx
