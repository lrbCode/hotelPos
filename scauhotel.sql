/*
 Navicat Premium Data Transfer

 Source Server         : myDB
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : scauhotel

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 26/06/2019 15:01:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for checkin
-- ----------------------------
DROP TABLE IF EXISTS `checkin`;
CREATE TABLE `checkin`  (
  `live_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '	\r\n入住单号(自增)',
  `rdid` int(11) NOT NULL COMMENT '房间具体信息外键，查金额和房间号  一对一',
  `cash_pledge` float(11, 2) NOT NULL COMMENT '押金',
  `live_nums` int(11) NOT NULL COMMENT '入住人数',
  `order_date` datetime(0) NOT NULL COMMENT '当前入住日期(天),钟点房精确到小时',
  `leave_date` datetime(0) NOT NULL COMMENT '当前退房日期(天),钟点房精确到小时',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `del_flag` tinyint(4) NULL DEFAULT 0 COMMENT '删除标记 默认0正在入住 1已退房',
  `payment` float(11, 2) NULL DEFAULT NULL COMMENT '支付金额(方便计算钟点房)',
  `pay_status` tinyint(4) NULL DEFAULT 0 COMMENT '支付状态 0未支付 1已支付',
  PRIMARY KEY (`live_id`) USING BTREE,
  INDEX `rdid`(`rdid`) USING BTREE,
  CONSTRAINT `rdid` FOREIGN KEY (`rdid`) REFERENCES `room_detail` (`room_number`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单信息\\入住登记表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of checkin
-- ----------------------------
INSERT INTO `checkin` VALUES (1, 102, 1.00, 1, '2019-05-16 00:30:09', '2019-05-17 00:30:12', '1', 1, 1.00, 1);
INSERT INTO `checkin` VALUES (2, 101, 2.00, 2, '2019-05-17 00:36:51', '2019-05-25 00:36:55', '2', 1, 2.00, 0);
INSERT INTO `checkin` VALUES (3, 101, 12.00, 2, '2019-05-24 00:38:18', '2019-05-30 00:38:22', '1', 1, 3.00, 0);
INSERT INTO `checkin` VALUES (4, 101, 123.00, 1, '2019-05-07 00:00:00', '2019-05-12 00:00:00', NULL, 0, NULL, 0);
INSERT INTO `checkin` VALUES (5, 102, 123.00, 1, '2019-05-07 00:00:00', '2019-05-12 00:00:00', NULL, 0, NULL, 0);
INSERT INTO `checkin` VALUES (6, 101, 123.00, 1, '2019-05-07 00:00:00', '2019-05-12 00:00:00', '123', 0, NULL, 0);
INSERT INTO `checkin` VALUES (9, 102, 123.00, 1, '2019-05-07 00:00:00', '2019-05-12 00:00:00', '123', 0, NULL, 0);

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客户ID(自增)',
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户姓名',
  `sex` tinyint(4) NOT NULL COMMENT '1男 0女',
  `customer_card` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户身份证',
  `age` int(11) NOT NULL COMMENT '客户年龄',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `check_id` int(11) NULL DEFAULT NULL COMMENT '入住订单外键',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `check_id`(`check_id`) USING BTREE,
  CONSTRAINT `check_id` FOREIGN KEY (`check_id`) REFERENCES `checkin` (`live_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '入住客户登记\\客户注册记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (1, '123', '123', 1, '123', 45, NULL, 1);
INSERT INTO `customer` VALUES (2, '123', '123', 1, '123', 45, NULL, 1);
INSERT INTO `customer` VALUES (3, '12', '12', 1, '4', 18, NULL, 1);
INSERT INTO `customer` VALUES (4, '123', '213456', 1, '213456', 12, NULL, 1);
INSERT INTO `customer` VALUES (5, '123', '12', 1, '123', 18, NULL, 1);
INSERT INTO `customer` VALUES (11, '123', '123', 1, '123', 45, NULL, 1);

-- ----------------------------
-- Table structure for pos_menupermission
-- ----------------------------
DROP TABLE IF EXISTS `pos_menupermission`;
CREATE TABLE `pos_menupermission`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单ID',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限标识',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1015 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pos_menupermission
-- ----------------------------
INSERT INTO `pos_menupermission` VALUES (1, '系统管理', 0, '#', 'M', '0', '', '系统管理目录');
INSERT INTO `pos_menupermission` VALUES (100, '用户管理', 0, '/system', 'C', '0', 'system:user:view', '用户管理菜单');
INSERT INTO `pos_menupermission` VALUES (101, '角色管理', 0, '/role', 'C', '0', 'system:role:view', '角色管理菜单');
INSERT INTO `pos_menupermission` VALUES (102, '客房价格管理', 0, '/roomPrice', 'C', '0', 'system:roomPrice:view', '');
INSERT INTO `pos_menupermission` VALUES (1000, '用户查询', 100, '#', 'F', '0', 'system:user:list', '');
INSERT INTO `pos_menupermission` VALUES (1001, '用户新增', 100, '#', 'F', '0', 'system:user:add', '');
INSERT INTO `pos_menupermission` VALUES (1002, '用户修改', 100, '#', 'F', '0', 'system:user:edit', '');
INSERT INTO `pos_menupermission` VALUES (1003, '用户删除', 100, '#', 'F', '0', 'system:user:remove', '');
INSERT INTO `pos_menupermission` VALUES (1007, '角色查询', 101, '#', 'F', '0', 'system:role:list', '');
INSERT INTO `pos_menupermission` VALUES (1008, '角色新增', 101, '#', 'F', '0', 'system:role:add', '');
INSERT INTO `pos_menupermission` VALUES (1009, '角色修改', 101, '#', 'F', '0', 'system:role:edit', '');
INSERT INTO `pos_menupermission` VALUES (1010, '角色删除', 101, '#', 'F', '0', 'system:role:remove', '');
INSERT INTO `pos_menupermission` VALUES (1011, '价格查询', 102, '#', 'F', '0', 'system:roomPrice:list', '');
INSERT INTO `pos_menupermission` VALUES (1012, '价格增加', 102, '#', 'F', '0', 'system:roomPrice:add', '');
INSERT INTO `pos_menupermission` VALUES (1013, '价格编辑', 102, '#', 'F', '0', 'system:roomPrice:edit', '');
INSERT INTO `pos_menupermission` VALUES (1014, '价格删除', 102, '#', 'F', '0', 'system:roomPrice:remove', '');

-- ----------------------------
-- Table structure for pos_role
-- ----------------------------
DROP TABLE IF EXISTS `pos_role`;
CREATE TABLE `pos_role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pos_role
-- ----------------------------
INSERT INTO `pos_role` VALUES (1, '管理员', 'admin', '0', '0', '管理员');
INSERT INTO `pos_role` VALUES (3, '前台用户', 'operator', '0', '0', '前台用户');

-- ----------------------------
-- Table structure for pos_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `pos_role_menu`;
CREATE TABLE `pos_role_menu`  (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pos_role_menu
-- ----------------------------
INSERT INTO `pos_role_menu` VALUES (3, 1);
INSERT INTO `pos_role_menu` VALUES (3, 101);
INSERT INTO `pos_role_menu` VALUES (3, 1007);
INSERT INTO `pos_role_menu` VALUES (3, 1008);
INSERT INTO `pos_role_menu` VALUES (3, 1009);
INSERT INTO `pos_role_menu` VALUES (3, 1010);

-- ----------------------------
-- Table structure for pos_user
-- ----------------------------
DROP TABLE IF EXISTS `pos_user`;
CREATE TABLE `pos_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `login_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '盐加密',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（默认 0管理员用户 1前台用户）',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 1代表删除）',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pos_user
-- ----------------------------
INSERT INTO `pos_user` VALUES (1, 'admin', '29c67a30398638269fe600f73a054934', '111111', '爸爸', '0', '15888888888', '1', '0', '管理员', '0');
INSERT INTO `pos_user` VALUES (2, 'ry', '8e6d98b90472783cc73c17047ddccf36', '222222', '若依', '0', '15666666666', '1', '0', '测试员', '0');
INSERT INTO `pos_user` VALUES (3, '12345', '5b192cb52ecc696f4432256b86254427', '910c94', 'randb', '0', '13760894690', '0', '0', '', '0');
INSERT INTO `pos_user` VALUES (4, 'dddd', 'ca70862c97596ae058db20bfa114769a', 'b42a79', 'dddd', '0', '17876892429', '0', '0', '', '0');
INSERT INTO `pos_user` VALUES (5, 'test', '06fc1fcbeca7fbcebb3b3dcc00e0220c', '8ab713', '测试', '0', '17876892425', '0', '0', '', '0');
INSERT INTO `pos_user` VALUES (6, 'testhaibin', '4802832d6abe497b85552a376eaaee5b', '8d5939', 'testhaibin', '0', '17876892423', '0', '0', '', '0');

-- ----------------------------
-- Table structure for pos_user_role
-- ----------------------------
DROP TABLE IF EXISTS `pos_user_role`;
CREATE TABLE `pos_user_role`  (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pos_user_role
-- ----------------------------
INSERT INTO `pos_user_role` VALUES (1, 1);
INSERT INTO `pos_user_role` VALUES (1, 3);
INSERT INTO `pos_user_role` VALUES (2, 2);
INSERT INTO `pos_user_role` VALUES (3, 3);
INSERT INTO `pos_user_role` VALUES (4, 2);
INSERT INTO `pos_user_role` VALUES (5, 3);
INSERT INTO `pos_user_role` VALUES (6, 2);
INSERT INTO `pos_user_role` VALUES (6, 3);

-- ----------------------------
-- Table structure for reserve
-- ----------------------------
DROP TABLE IF EXISTS `reserve`;
CREATE TABLE `reserve`  (
  `id` int(11) NOT NULL COMMENT '预定单号 随机生成',
  `phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `reservation_time` datetime(0) NOT NULL COMMENT '预订日期(天)',
  `go_time` datetime(0) NOT NULL COMMENT '预定退房日期(天)',
  `del_status` tinyint(4) NULL DEFAULT 0 COMMENT '状态(0已预订 1预定完成 ，新增预订not between',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`, `phone`) USING BTREE,
  INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '预订记录表，新预订不能在checkin时间段和预订记录时间段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reserve
-- ----------------------------
INSERT INTO `reserve` VALUES (-1844035626, '123', '2019-05-07 00:00:00', '2019-05-12 00:00:00', 1, NULL);
INSERT INTO `reserve` VALUES (1, '123456', '2019-04-23 15:59:24', '2019-04-23 15:59:21', 0, '1');
INSERT INTO `reserve` VALUES (2, '2', '2019-04-17 20:15:32', '2019-04-11 20:15:35', 0, '1');
INSERT INTO `reserve` VALUES (3, '1234', '2019-05-23 19:26:48', '2019-05-22 19:26:51', 0, '1');
INSERT INTO `reserve` VALUES (1321, '123', '2019-05-07 00:00:00', '2019-05-12 00:00:00', 0, NULL);

-- ----------------------------
-- Table structure for reserve_type
-- ----------------------------
DROP TABLE IF EXISTS `reserve_type`;
CREATE TABLE `reserve_type`  (
  `phone` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号',
  `rtid` int(11) NOT NULL COMMENT '房间类型外键',
  `order_num` int(11) NOT NULL COMMENT '预定数量',
  PRIMARY KEY (`phone`, `rtid`) USING BTREE,
  INDEX `rtid_res`(`rtid`) USING BTREE,
  CONSTRAINT `phone_id` FOREIGN KEY (`phone`) REFERENCES `reserve` (`phone`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reserve_type
-- ----------------------------
INSERT INTO `reserve_type` VALUES ('123', 1, 3);
INSERT INTO `reserve_type` VALUES ('123', 2, 2);
INSERT INTO `reserve_type` VALUES ('123456', 1, 0);
INSERT INTO `reserve_type` VALUES ('123456', 2, 0);
INSERT INTO `reserve_type` VALUES ('2', 1, 0);
INSERT INTO `reserve_type` VALUES ('2', 2, 0);

-- ----------------------------
-- Table structure for room_detail
-- ----------------------------
DROP TABLE IF EXISTS `room_detail`;
CREATE TABLE `room_detail`  (
  `room_number` int(11) NOT NULL COMMENT '房间号主键',
  `rtid` int(11) NULL DEFAULT NULL COMMENT '房间类型ID外键',
  `clock_room` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否为钟点房状态 默认0否 1是',
  `clock_price` float(10, 2) NULL DEFAULT NULL COMMENT '钟点房，价格\\小时',
  `room_tel` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客房联系电话',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `cur_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '当前房间状态 默认0(空房) 1入住 ',
  PRIMARY KEY (`room_number`) USING BTREE,
  INDEX `rtid`(`rtid`) USING BTREE,
  CONSTRAINT `rtid` FOREIGN KEY (`rtid`) REFERENCES `room_type` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '客房当前详情表,具体到房间号，一间房可以住多个客户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of room_detail
-- ----------------------------
INSERT INTO `room_detail` VALUES (101, 1, 0, 60.00, '11110000', '非常好', 2);
INSERT INTO `room_detail` VALUES (102, 2, 0, 70.00, '11111111', '非常差', 0);

-- ----------------------------
-- Table structure for room_price
-- ----------------------------
DROP TABLE IF EXISTS `room_price`;
CREATE TABLE `room_price`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `rtid` int(11) NULL DEFAULT NULL COMMENT '房间类型外键',
  `price` float(10, 2) NOT NULL COMMENT '客房类型价格',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '价格时间(入住那一天)',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `rtid_price`(`rtid`) USING BTREE,
  CONSTRAINT `rtid_price` FOREIGN KEY (`rtid`) REFERENCES `room_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '房间价格表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of room_price
-- ----------------------------
INSERT INTO `room_price` VALUES (1, 1, 1000.00, '2019-03-26 00:00:00', '3-26号价格');
INSERT INTO `room_price` VALUES (2, 1, 12.00, '2019-05-27 00:00:00', NULL);

-- ----------------------------
-- Table structure for room_type
-- ----------------------------
DROP TABLE IF EXISTS `room_type`;
CREATE TABLE `room_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `room_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客房类型',
  `empty_num` int(11) NULL DEFAULT NULL COMMENT '空房数量',
  `remark` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '客房类型介绍',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = '客房类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of room_type
-- ----------------------------
INSERT INTO `room_type` VALUES (1, '单人房', 100, '特价单人房，设备齐全');
INSERT INTO `room_type` VALUES (2, '双人房', 100, '就很贵');
INSERT INTO `room_type` VALUES (3, 'sad', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
